<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/PatternDecorationRequireInvalidFormatException.php');
include($strRootPath . '/src/config/exception/PatternDecorationInvalidFormatException.php');
include($strRootPath . '/src/config/exception/ParamStartInvalidFormatException.php');
include($strRootPath . '/src/config/exception/ParamEndInvalidFormatException.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/route/library/ConstRoute.php');
include($strRootPath . '/src/route/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/route/exception/RouteCollectionInvalidFormatException.php');
include($strRootPath . '/src/route/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/route/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/route/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/route/api/RouteInterface.php');
include($strRootPath . '/src/route/api/RouteCollectionInterface.php');
include($strRootPath . '/src/route/model/DefaultRoute.php');
include($strRootPath . '/src/route/model/DefaultRouteCollection.php');

include($strRootPath . '/src/route/pattern/library/ConstPatternRoute.php');
include($strRootPath . '/src/route/pattern/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/route/pattern/model/PatternRoute.php');

include($strRootPath . '/src/route/param/library/ConstParamRoute.php');
include($strRootPath . '/src/route/param/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/route/param/model/ParamRoute.php');

include($strRootPath . '/src/route/fix/library/ConstFixRoute.php');
include($strRootPath . '/src/route/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/route/fix/model/FixRoute.php');

include($strRootPath . '/src/route/separator/library/ConstSeparatorRoute.php');
include($strRootPath . '/src/route/separator/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/route/separator/model/SeparatorRoute.php');

include($strRootPath . '/src/route/factory/library/ConstRouteFactory.php');
include($strRootPath . '/src/route/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/route/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/route/factory/api/RouteFactoryInterface.php');
include($strRootPath . '/src/route/factory/model/DefaultRouteFactory.php');

include($strRootPath . '/src/route/factory/standard/library/ConstStandardRouteFactory.php');
include($strRootPath . '/src/route/factory/standard/model/StandardRouteFactory.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');

include($strRootPath . '/src/router/library/ConstRouter.php');
include($strRootPath . '/src/router/exception/RouteCollectionInvalidFormatException.php');
include($strRootPath . '/src/router/exception/RouteNotFoundException.php');
include($strRootPath . '/src/router/exception/SourceUnableGetException.php');
include($strRootPath . '/src/router/exception/CallableUnableGetException.php');
include($strRootPath . '/src/router/api/RouterInterface.php');
include($strRootPath . '/src/router/model/DefaultRouter.php');

include($strRootPath . '/src/call/call/library/ConstCall.php');
include($strRootPath . '/src/call/call/exception/RouterInvalidFormatException.php');
include($strRootPath . '/src/call/call/model/DefaultCall.php');

include($strRootPath . '/src/call/call/route/library/ConstRouteCall.php');
include($strRootPath . '/src/call/call/route/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/call/route/model/RouteCall.php');

include($strRootPath . '/src/call/call/factory/library/ConstRouteCallFactory.php');
include($strRootPath . '/src/call/call/factory/model/RouteCallFactory.php');