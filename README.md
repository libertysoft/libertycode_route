LibertyCode_Route
=================



Description
-----------

Library contains routing components, 
to use router system to run executable, 
from specified source.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/route ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/route": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Main configuration

- Use following class to configure specific elements
    
    ```php
    use liberty_code\route\config\model\DefaultConfig;
    DefaultConfig::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Regular expression options, used in routes
    - Arguments options, used in routes
    - Global information

---



Usage
-----

#### Route

Routes system allows to get executable, 
from specific configuration, 
and specified source.

_Elements_

- Route

    Allows to design a route, 
    who is an item containing configuration array, 
    to get executable (callback function) 
    from specified string source.

- PatternRoute

    Extends route features. 
    Uses regular expressions in configuration array.
    
- ParamRoute

    Extends route features. 
    Uses source with parameters in configuration array.
    
- FixRoute

    Extends route features. 
    Uses fix configuration in configuration array.
    
- SeparatorRoute

    Extends route features. 
    Uses separator configuration in configuration array.

- RouteCollection

    Allows to design collection of routes.
    Uses list of routes, 
    to retrieve executable, 
    from specified route or string source.

- RouteFactory

    Allows to design a route factory, 
    to provide new or specified route instance,
    from specified configuration.
    
- DiRouteFactory

    Extends route factory features. 
    Provides new route instance, 
    from specified DI provider.
    
- StandardRouteFactory

    Extends DI route factory features. 
    Provides route instance.
    
_Example_

```php
// Get route factory
use liberty_code\route\route\factory\standard\model\StandardRouteFactory;
$routeFactory = new StandardRouteFactory($provider);
...
// Get new route from configuration
$route = $routeFactory->getObjRoute(array(...));
...
```

#### Builder

Builder allows to hydrate route collection, with routes.

_Elements_

- DefaultBuilder
    
    Uses array of source data to hydrate route collection.

_Example_

```php
// Get route collection
use liberty_code\route\route\model\DefaultRouteCollection;
$routeCollection = new DefaultRouteCollection();
...
// Get route builder
use liberty_code\route\build\model\DefaultBuilder;
$routeBuilder = new DefaultBuilder($routeFactory);
...
// Hydrate route collection
$routeBuilder->setTabDataSrc(array(...));
$routeBuilder->hydrateRouteCollection($routeCollection);
...
foreach($routeCollection->getTabKey() as $key) {
    echo($routeCollection->getObjRoute($key)->getStrKey() .'<br />');
}
/**
 * Show: 
 * Route key 1
 * ...
 * Route key N
 */
...
```

#### Router

Router allows to run executable(s) (callback function(s)), 
using route collection.

_Example_

```php
use liberty_code\route\router\model\DefaultRouter;
$router = new DefaultRouter();
$router->setRouteCollection(...);
...
// Run executable(s), from specified string source
var_dump($router->execute(...));
...
```

#### Call

Calls using route system,
to get callback function, 
to execute specific route destination.

_Elements_

- DefaultCall

    Extends call features. 
    Uses router, 
    to get executable,
    to execute specific destination.
    
- RouteCall

    Extends default route call features. 
    Allows to configure route destination.
    
- RouteCallFactory

    Extends call factory features. 
    Provides route call instance.
    
---


