<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/router/test/RouterTest.php');

// Use
use liberty_code\call\call\library\ConstCall;
use liberty_code\route\call\call\route\library\ConstRouteCall;
use liberty_code\route\call\call\route\model\RouteCall;



// Init var
$tabDataSrc = array(
    'route_1_not_care' => [
        'type' => 'pattern',
        'key' => 'route_1',
        'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
        'argument' => 'arg_\d/([^/]+)',
        'call' => [
            'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ],
        'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/'
    ],
    'route_2:[strAdd2]:test_1' => [
        'argument' => [
            'start' => '[',
            'end' => ']'
        ],
        'call' => [
            'type' => 'dependency',
            'dependency_key_pattern' => 'svc_1:action'
        ]
    ],
    'route_3' => [
        'type' => 'fix',
        'key' => 'route_3',
        'source' => 'route_3/test_1/test_2/test_3',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => 'route/test/FileControllerTest1.php',
            'file_path_format_require' => 7
        ]
    ],
    [
        'type' => 'separator',
        'key' => 'route_4',
        'separator' => '/',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => '/route/test/%1$sController%2$s.php',
            'file_path_format_require' => true
        ]
    ],
    'route_5' => [
        'type' => 'pattern',
        'source' => '^route_5/controller/[^/]+/action/[^/]+/$',
        'element' => 'controller/([^/]+)/action/([^/]+)',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\route\\route\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
        ],
        'option' => [
            'decoration' => '5'
        ]
    ],
    [
        'type' => 'separator',
        'separator' => '/',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => '/route/test/%1$sController%2$s.php',
            'file_path_format_require' => true
        ]
    ]
);

$objRouteCollection->setMultiMatch(false);
$objRouteCollection->setMultiMatch(true);

$objRouteBuilder->setTabDataSrc($tabDataSrc);
$objRouteBuilder->hydrateRouteCollection($objRouteCollection);

$tabConfig = array(
    ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_KEY_VALUE,
    ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN => '<%1$s>'
);

$callableBefore = function (array $tabParam) {
    echo('Test callable before:<pre>');var_dump($tabParam);echo('</pre>');
};

$callableAfter = function (array $tabParam) {
    echo('Test callable after: <pre>');var_dump($tabParam);echo('</pre>');
};

$objCall = new RouteCall(
    $objRouter,
    $tabConfig,
    null,
    $callableBefore,
    $callableAfter
);



// Test call
$tabCallData = array(
    [
        [
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_1'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        []
    ], // Ok

    [
        [
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_test'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        []
    ], // Ko: route not found

    [
        [
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_<elm_key_1>'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => '1'
        ]
    ], // Ok

    [
        [
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_%1$s'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => '1'
        ],
        [
            ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_INDEX,
            ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN => '<%1$s>'
        ]
    ], // Ko: Bad configuration format

    [
        [
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_%1$s'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => '1'
        ],
        [
            ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_INDEX
        ]
    ] // Ok
);

foreach($tabCallData as $callData)
{
    echo('Test call: <br />');
    echo('<pre>');var_dump($callData);echo('</pre>');

    try{
        $tabCallConfig = $callData[0];
        $tabArg = $callData[1];
        $tabStrElm = $callData[2];

        $tabConfigPrevious = null;
        if(isset($callData[3]))
        {
            $tabConfigPrevious = $objCall->getTabConfig();
            $tabConfig = $callData[3];

            $objCall->setConfig($tabConfig);
            echo('Config: <pre>');var_dump($objCall->getTabConfig());echo('</pre>');
        }

        $objCall->setCallConfig($tabCallConfig);
        echo('Call config: <pre>');var_dump($objCall->getTabCallConfig());echo('</pre>');

        $callable = $objCall->getCallable($tabArg, $tabStrElm);
        echo('Call:<br />');print_r($callable());
        echo('<br /><br />');

        $callable = $objCall->getCallable($tabArg, $tabStrElm, false, false);
        echo('Call (without before, after):<br />');print_r($callable());

        if(!is_null($tabConfigPrevious))
        {
            $objCall->setConfig($tabConfigPrevious);
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


