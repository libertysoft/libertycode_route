<?php
/**
 * Description :
 * This class allows to define route call class.
 * Route call is a default route call, allows to configure route destination.
 *
 * Route call uses the following specified configuration:
 * [
 *     Default route call configuration
 * ]
 *
 * Route call uses the following specified destination configuration:
 * [
 *     route_key_pattern(required):
 *         "route key pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\call\call\route\model;

use liberty_code\route\call\call\model\DefaultCall;

use liberty_code\call\call\library\ConstCall;
use liberty_code\route\call\call\route\library\ConstRouteCall;
use liberty_code\route\call\call\route\exception\CallConfigInvalidFormatException;



class RouteCall extends DefaultCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG:
                    CallConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws CallConfigInvalidFormatException
     */
    public function getCallable(
        array $tabArg = array(),
        array $tabStrElm = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    )
    {
        // Init var
        $result = null;
        $objRouter = $this->getObjRouter();
        $tabCallConfig = $this->getTabCallConfig();

        // Check minimal requirement
        if((!is_null($objRouter)) && (count($tabCallConfig) > 0))
        {
            // Get infos from config
            $strRouteKey = $tabCallConfig[ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN]; // Get route key

            // Check config info valid
            if(
                (!is_null($strRouteKey)) &&
                is_string($strRouteKey) &&
                (trim($strRouteKey) != '')
            )
            {
                // Get calculated route key
                $strRouteKey = @$this->getStrValueFormatFromElm($strRouteKey, $tabStrElm);

                // Check route found
                if ($objRouter->checkExists($strRouteKey))
                {
                    // Set callback function
                    $result = function () use ($objRouter, $strRouteKey, $tabStrElm, $tabArg)
                    {
                        // Return result
                        return $objRouter->executeRoute($strRouteKey, $tabArg, $tabStrElm);
                    };

                    // Wrap callback function
                    $result = $this->getCallableWrap
                    (
                        $result,
                        array(
                            ConstRouteCall::TAB_CALLABLE_PARAM_ARG_KEY_ROUTE_KEY => $strRouteKey,
                            ConstRouteCall::TAB_CALLABLE_PARAM_ARG_KEY_CALL_ELEMENT => $tabStrElm,
                            ConstRouteCall::TAB_CALLABLE_PARAM_ARG_KEY_CALL_ARGUMENT => $tabArg
                        ),
                        $boolCallBefore,
                        $boolCallAfter
                    );
                }
            }

            // Throw exception, if callable not gettable
            if(is_null($result))
            {
                throw new CallConfigInvalidFormatException(serialize($this->getTabCallConfig()));
            }
        }

        // Return result
        return $result;
    }



}