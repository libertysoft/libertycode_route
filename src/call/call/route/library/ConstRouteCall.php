<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\call\call\route\library;



class ConstRouteCall
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Destination configuration
    const TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN = 'route_key_pattern';

    // Callable parameters configuration
    const TAB_CALLABLE_PARAM_ARG_KEY_ROUTE_KEY = 'route_key';
    const TAB_CALLABLE_PARAM_ARG_KEY_CALL_ELEMENT = 'element';
    const TAB_CALLABLE_PARAM_ARG_KEY_CALL_ARGUMENT = 'argument';



    // Exception message constants
    const EXCEPT_MSG_CALL_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the route call destination configuration standard.';
}