<?php
/**
 * Description :
 * This class allows to define default route call class.
 * Default route call is a call, allows to execute specific configured destination,
 * from specified router.
 *
 * Default route call uses the following specified configuration:
 * [
 *     Default call configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\call\call\model;

use liberty_code\call\call\model\DefaultCall as BaseDefaultCall;

use liberty_code\route\router\api\RouterInterface;
use liberty_code\route\call\call\library\ConstCall;
use liberty_code\route\call\call\exception\RouterInvalidFormatException;



/**
 * @method RouterInterface getObjRouter() Get router object.
 * @method void setObjRouter(RouterInterface $objProvider) Set router object.
 */
abstract class DefaultCall extends BaseDefaultCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RouterInterface $objRouter
     */
    public function __construct(
        RouterInterface $objRouter,
        array $tabConfig = null,
        array $tabCallConfig = null,
        callable $callableBefore = null,
        callable $callableAfter = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $tabCallConfig,
            $callableBefore,
            $callableAfter
        );

        // Init router
        $this->setObjRouter($objRouter);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCall::DATA_KEY_DEFAULT_ROUTER))
        {
            $this->__beanTabData[ConstCall::DATA_KEY_DEFAULT_ROUTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCall::DATA_KEY_DEFAULT_ROUTER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_ROUTER:
                    RouterInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



}