<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\call\call\library;



class ConstCall
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_ROUTER = 'objRouter';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_ROUTER_INVALID_FORMAT = 'Following router "%1$s" invalid! It must be a router object.';
}