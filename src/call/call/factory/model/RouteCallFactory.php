<?php
/**
 * Description :
 * This class allows to define route call factory class.
 * Route call factory allows to provide and hydrate route call instance.
 *
 * Route call factory uses the following specified configuration, to hydrate call:
 * [
 *     Route call configuration (@see RouteCall )
 * ]
 *
 * Route call factory uses the following specified destination configuration, to get and hydrate call:
 * [
 *     type(optional): "route",
 *     Route call destination configuration (@see RouteCall )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\call\call\factory\model;

use liberty_code\call\call\factory\model\DefaultCallFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\call\call\api\CallInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\route\router\api\RouterInterface;
use liberty_code\route\call\call\exception\RouterInvalidFormatException;
use liberty_code\route\call\call\route\model\RouteCall;
use liberty_code\route\call\call\factory\library\ConstRouteCallFactory;



/**
 * @method RouterInterface getObjRouter() Get router object.
 * @method void setObjRouter(RouterInterface $objProvider) Set router object.
 */
class RouteCallFactory extends DefaultCallFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RouterInterface $objRouter
     */
    public function __construct(
        RouterInterface $objRouter,
        array $tabConfig = null,
        $callableBefore = null,
        $callableAfter = null,
        CallFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $callableBefore,
            $callableAfter,
            $objFactory,
            $objProvider
        );

        // Init router
        $this->setObjRouter($objRouter);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRouteCallFactory::DATA_KEY_DEFAULT_ROUTER))
        {
            $this->__beanTabData[ConstRouteCallFactory::DATA_KEY_DEFAULT_ROUTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateCall(CallInterface $objCall, array $tabCallConfig)
    {
        // Hydrate DI call, if required
        if($objCall instanceof RouteCall)
        {
            $objCall->setObjRouter($this->getObjRouter());
        }

        // Return result
        return parent::hydrateCall($objCall, $tabCallConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRouteCallFactory::DATA_KEY_DEFAULT_ROUTER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRouteCallFactory::DATA_KEY_DEFAULT_ROUTER:
                    RouterInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrCallClassPathFromType($strCallConfigType)
    {
        // Init var
        $result = null;

        // Get class path of call, from type
        switch($strCallConfigType)
        {
            case null:
            case ConstRouteCallFactory::CALL_CONFIG_TYPE_ROUTE:
                $result = RouteCall::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjCallNew($strCallConfigType)
    {
        // Init var
        $result = null;
        $objRouter = $this->getObjRouter();

        // Get class path of call, from type
        switch($strCallConfigType)
        {
            case null:
            case ConstRouteCallFactory::CALL_CONFIG_TYPE_ROUTE:
                $result = new RouteCall($objRouter);
                break;
        }

        // Return result
        return $result;
    }



}