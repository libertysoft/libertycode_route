<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\call\call\factory\library;



class ConstRouteCallFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_ROUTER = 'objRouter';



    // Type configuration
    const CALL_CONFIG_TYPE_ROUTE = 'route';
}