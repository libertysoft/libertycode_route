<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/call/call/factory/test/CallFactoryTest.php');

// Use
use liberty_code\call\call\file\model\FileCall;
use liberty_code\call\call\factory\library\ConstCallFactory;
use liberty_code\route\call\call\route\library\ConstRouteCall;
use liberty_code\route\call\call\route\model\RouteCall;
use liberty_code\route\call\call\factory\library\ConstRouteCallFactory;



// Init var
$tabDataSrc = array(
    'route_1' => [
        'type' => 'pattern',
        'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
        'argument' => 'arg_\d/([^/]+)',
        'call' => [
            'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ],
        'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/'
    ]
);

$objRouteBuilder->setTabDataSrc($tabDataSrc);
$objRouteBuilder->hydrateRouteCollection($objRouteCollection);



// Test new call
$tabCallData = array(
    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstRouteCallFactory::CALL_CONFIG_TYPE_ROUTE,
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_1'
        ]
    ], // Ok

    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstRouteCallFactory::CALL_CONFIG_TYPE_ROUTE,
            'test' => 'action'
        ]
    ], // Ko: Function call invalid

    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstRouteCallFactory::CALL_CONFIG_TYPE_ROUTE,
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_1'
        ],
        new RouteCall($objRouter)
    ], // Ok

    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstRouteCallFactory::CALL_CONFIG_TYPE_ROUTE,
            ConstRouteCall::TAB_CALL_CONFIG_KEY_ROUTE_KEY_PATTERN => 'route_1'
        ],
        new FileCall()
    ] // Ko: not found: file call used for route call config
);

foreach($tabCallData as $callData)
{
    echo('Test new call: <br />');
    echo('<pre>');var_dump($callData);echo('</pre>');

    try{
        $tabCallConfig = $callData[0];
        $objCall = (isset($callData[1]) ? $callData[1] : null);
        $objCall = $objCallFactory->getObjCall($tabCallConfig, $objCall);

        echo('Class path: <pre>');var_dump($objCallFactory->getStrCallClassPath($tabCallConfig));echo('</pre>');

        if(!is_null($objCall))
        {
            echo('Call class path: <pre>');var_dump(get_class($objCall));echo('</pre>');
            echo('Call config: <pre>');var_dump($objCall->getTabConfig());echo('</pre>');
            echo('Call destination config: <pre>');var_dump($objCall->getTabCallConfig());echo('</pre>');
            echo('Call callable before: <pre>');var_dump($objCall->getCallableBefore());echo('</pre>');
            echo('Call callable after: <pre>');var_dump($objCall->getCallableAfter());echo('</pre>');
        }
        else
        {
            echo('Call not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


