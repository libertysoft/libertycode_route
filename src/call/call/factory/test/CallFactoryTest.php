<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/router/test/RouterTest.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\route\call\call\factory\model\RouteCallFactory;



// Init var
$objPref = new Preference(array(
    'source' => 'liberty_code\\route\\router\\api\\RouterInterface',
    'set' =>  ['type' => 'instance', 'value' => $objRouter],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

$objCallFactory = new RouteCallFactory(
    $objRouter,
    $objCallFactory->getTabConfig(),
    $objCallFactory->getCallableBefore(),
    $objCallFactory->getCallableBefore(),
    $objCallFactory
);


