<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\build\exception;

use liberty_code\route\build\library\ConstBuilder;



class DataSrcInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $dataSrc
     */
	public function __construct($dataSrc)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
			ConstBuilder::EXCEPT_MSG_DATA_SRC_INVALID_FORMAT,
			mb_strimwidth(strval($dataSrc), 0, 10, "...")
		);
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified data source has valid format.
	 * 
     * @param mixed $dataSrc
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($dataSrc)
    {
		// Init var
		$result = is_array($dataSrc);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($dataSrc) ? serialize($dataSrc) : $dataSrc));
		}
		
		// Return result
		return $result;
    }
	
	
	
}