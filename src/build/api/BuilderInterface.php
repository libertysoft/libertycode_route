<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified route collection instance, with routes.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\build\api;

use liberty_code\route\route\api\RouteCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified route collection.
     *
     * @param RouteCollectionInterface $objRouteCollection
     * @param boolean $boolClear = true
     */
    public function hydrateRouteCollection(RouteCollectionInterface $objRouteCollection, $boolClear = true);
}