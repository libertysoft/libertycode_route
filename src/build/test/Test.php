<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/RouteBuilderTest.php');

// Use
use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\route\model\DefaultRouteCollection;



// Init var
$objRouteCollection = new DefaultRouteCollection($objCallFactory);

$tabDataSrc = array(
    'route_1_not_care' => [
        'type' => 'pattern',
        'key' => 'route_1',
        'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
        'argument' => 'arg_\d/([^/]+)',
        'call' => [
            'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ],
        'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/'
    ],
    'route_2:[strAdd2]:test_1' => [
        'argument' => [
            'start' => '[',
            'end' => ']'
        ],
        'call' => [
            'type' => 'dependency',
            'dependency_key_pattern' => 'svc_1:action'
        ]
    ],
    'route_3' => [
        'type' => 'fix',
        'key' => 'route_3',
        'source' => 'route_3/test_1/test_2/test_3',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => 'src/route/test/FileControllerTest1.php',
            'file_path_format_require' => 7
        ]
    ],
	[
        'type' => 'separator',
        'key' => 'route_4',
        'separator' => '/',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
            'file_path_format_require' => true
        ]
	],
    'route_5' => [
        'type' => 'pattern',
        'source' => '^route_5/controller/[^/]+/action/[^/]+/$',
        'element' => 'controller/([^/]+)/action/([^/]+)',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\route\\route\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
        ],
        'option' => [
            'decoration' => '5'
        ]
    ],
    [
        'type' => 'separator',
        'separator' => '/',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
            'file_path_format_require' => true
        ]
    ]
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objRouteBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objRouteBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objRouteBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test hydrate route collection
echo('Test hydrate route collection: <br />');

$objRouteBuilder->hydrateRouteCollection($objRouteCollection, false);
foreach($objRouteCollection as $strKey => $objRoute)
{
    /** @var RouteInterface $objRoute */
	try{
		echo('Route "' . $strKey . '":');echo('<br />');
		echo('Get: key: <pre>');print_r($objRoute->getStrKey());echo('</pre>');
		echo('Get: config: <pre>');print_r($objRoute->getTabConfig());echo('</pre>');
		echo('Get: route collection count: <pre>');print_r(count($objRoute->getObjRouteCollection()->getTabKey()));echo('</pre>');
        echo('Get: instance: <pre>');var_dump(get_class($objRoute));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');



// Test clear route collection
echo('Test clear: <br />');

echo('Before clearing: <pre>');print_r($objRouteCollection->getTabKey());echo('</pre>');

$objRouteBuilder->setTabDataSrc(array());
$objRouteBuilder->hydrateRouteCollection($objRouteCollection);
echo('After clearing: <pre>');print_r($objRouteCollection->getTabKey());echo('</pre>');

echo('<br /><br /><br />');


