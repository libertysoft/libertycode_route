<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/route/factory/test/RouteFactoryTest.php');

// Use
use liberty_code\route\build\model\DefaultBuilder;

$objRouteBuilder = new DefaultBuilder(
    $objRouteFactory
);


