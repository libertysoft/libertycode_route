<?php

namespace liberty_code\route\route\test;



class ClassTest1
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var string */
    protected $strArg1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param string $strArg1 = 'class test 1'
     */
    public function __construct($strArg1 = 'class test 1')
    {
        // Init var
		$this->strArg1 = $strArg1;
    }





    // Methods getters
    // ******************************************************************************

    public function getStrArg1()
    {
        // Return result
        return $this->strArg1;
    }
	
	
	
	
	
	// Methods action
    // ******************************************************************************

    public function action($strAdd = '', $strAdd2 = '')
    {
        // Return result
        return 'action class test 1' . 
		((trim($strAdd) != '') ? ' - 1: ' . $strAdd : '').
		((trim($strAdd2) != '') ? ' - 2: ' . $strAdd2 : '');
    }
}