<?php

namespace liberty_code\route\route\test;



class ControllerTest1
{
    
	// Constructor / Destructor
    // ******************************************************************************

	/**
     * Constructor
     */
	/*
    public function __construct()
    {
        
    }
	//*/
	
	
	
	
	
	// Methods action
    // ******************************************************************************

    /**
     * @param string $strAdd
     * @param string $strAdd2
     * @return string
     */
    public function action($strAdd = '', $strAdd2 = '')
    {
        // Return result
        return 'action controller test 1' . 
		((trim($strAdd) != '') ? ': ' . $strAdd : ''). 
		((trim($strAdd2) != '') ? ': ' . $strAdd2 : '');
    }
}