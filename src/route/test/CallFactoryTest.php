<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/route/test/ClassTest1.php');
require_once($strRootAppPath . '/src/route/test/ControllerTest1.php');
require_once($strRootAppPath . '/src/route/test/ControllerTest2.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\factory\standard\model\StandardDependencyFactory;
use liberty_code\di\build\model\DefaultBuilder;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\call\call\file\library\ConstFileCall;
use liberty_code\call\call\di\func\library\ConstFunctionCall;
use liberty_code\call\call\factory\file\model\FileCallFactory;
use liberty_code\call\call\factory\di\model\DiCallFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objDepFactory = new StandardDependencyFactory();
$objDepBuilder = new DefaultBuilder($objDepFactory);

$tabDataSrc = array(
    'svc_1' => [
        'source' => 'liberty_code\\route\\route\\test\\ClassTest1',
        'argument' => [
            ['type' => 'string', 'value' => 'class test 1 update']
        ],
        'option' => [
            //'shared' => true,
            //'class_wired' => 0
        ]
    ]
);

$objDepBuilder->setTabDataSrc($tabDataSrc);
$objDepBuilder->hydrateDependencyCollection($objDepCollection);
$objProvider = new DefaultProvider($objDepCollection);

$objPref = new Preference(array(
    'source' => 'liberty_code\\di\\provider\\api\\ProviderInterface',
    'set' =>  ['type' => 'instance', 'value' => $objProvider],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init call factory
$tabConfig = array(
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true,
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true
);

$callableBefore = function (array $tabParam) {
    echo('Test callable before:<pre>');var_dump($tabParam);echo('</pre>');
};

$callableAfter = function (array $tabParam) {
    echo('Test callable after: <pre>');var_dump($tabParam);echo('</pre>');
};

$objCallFactory = new FileCallFactory(
    $tabConfig,
    $callableBefore,
    $callableAfter
);

$objCallFactory = new DiCallFactory(
    $objProvider,
    $tabConfig,
    $callableBefore,
    $callableAfter,
    $objCallFactory
);


