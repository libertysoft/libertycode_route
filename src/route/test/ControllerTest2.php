<?php

namespace liberty_code\route\route\test;

use liberty_code\route\route\test\ClassTest1;



class ControllerTest2
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var ClassTest1 */
    protected $objArg1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param ClassTest1 $objArg1
     */
    public function __construct(ClassTest1 $objArg1)
    {
        // Init var
		$this->objArg1 = $objArg1;
    }

	
	
	
	
    // Methods action
    // ******************************************************************************

    public function action($strAdd = '')
    {
        // Return result
        return 'action controller test 2:' . $this->objArg1->getStrArg1() . ((trim($strAdd) != '') ? ': ' . $strAdd : '');
    }
}