<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/route/test/CallFactoryTest.php');

// Use
use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\route\pattern\model\PatternRoute;
use liberty_code\route\route\param\model\ParamRoute;
use liberty_code\route\route\fix\model\FixRoute;
use liberty_code\route\route\separator\model\SeparatorRoute;
use liberty_code\route\route\model\DefaultRouteCollection;



// Init var
$tabConfig = array(
    //'multi_match' => 1
);
$objRouteCollection = new DefaultRouteCollection($objCallFactory, $tabConfig);



// Test add route
echo('Test add : <br />');

$objRoute1 = new PatternRoute(array(
    'key' => 'route_1',
    'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
    'argument' => 'arg_\d/([^/]+)',
    'call' => [
        'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
        //'class_path_format_require' => true
    ],
    'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/',
    //'order'=> 1
));

$objRoute2 = new ParamRoute(array(
    'key' => 'route_2',
    //'source' => 'route_2:{arg_1}:test_1',
    //*
    'source' => 'route_2:[strAdd2]:test_1',
    'argument' => [
        'start' => '[',
        'end' => ']',
        'exclude_value' => ':'
    ],
    //*/
    'call' => [
        'type' => 'dependency',
        'dependency_key_pattern' => 'svc_1:action'
    ]
));

$objRoute3 = new FixRoute(array(
    'key' => 'route_3',
    'source' => 'route_3/test_1/test_2/test_3',
    'argument' => [
        'argument value 1'
    ],
    'call' => [
        'type' => 'file',
        'file_path_pattern' => 'src/route/test/FileControllerTest1.php',
        'file_path_format_require' => 7
    ]
));

$objRoute4 = new PatternRoute(array(
    'key' => 'route_4',
    'source' => '^route_4/arg_1/([^/]+)/',
    'call' => [
        'type' => 'function',
        'file_path_pattern' => 'src/route/test/FileControllerTest2.php:runAction',
        'file_path_format_require' => true
    ]
));

$objRoute5 = new PatternRoute(array(
    'key' => 'route_5',
    'source' => '^route_5/controller/[^/]+/action/[^/]+/$',
    'element' => 'controller/([^/]+)/action/([^/]+)',
    'argument' => [
        'argument value 1'
    ],
    'call' => [
        'type' => 'class',
        'class_path_pattern' => 'liberty_code\\route\\route\\test\\Controller%1$s',
        'method_name_pattern' => '%2$s'
    ],
    'option' => [
        'decoration' => '5'
    ]
));

$objRoute6 = new PatternRoute(array(
    'key' => 'route_6',
    'source' => '^route_6/key/([^/]+)/method/([^/]+)/',
    'argument' => '\\{([^\\}]+)\\}',
    'call' => [
        'type' => 'dependency',
        'dependency_key_pattern' => '%1$s',
        'method_pattern' => '%2$s'
    ],
    'option' => [
        'decoration' => true
    ],
    'build_source_pattern' => 'route_6/key/%1$s/method/%2$s/{%3$s}/build_test_1/{%4$s}'
));

$objRoute7 = new SeparatorRoute(array(
    'key' => 'route_7',
    'separator' => '/',
    'argument' => [
        'argument value 1'
    ],
    'call' => [
        'type' => 'file',
        'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
        'file_path_format_require' => true
    ]
));

$objRoute8 = new SeparatorRoute(array(
    'key' => 'route_8',
    'separator' => ':',
    'argument' => '/arg\\:([^\\:]+)/',
    'call' => [
        'type' => 'function',
        'function_name_pattern' => '%2$s',
        'file_path_pattern' => '/src/route/test/%1$s.php',
        'file_path_format_require' => 1
    ],
    'option' => [
        'decoration' => false
    ],
    'build_source_pattern' => '%1$s:%2$s:arg:%3$s:arg:%4$s:build_test_1'
));

$objRoute9 = new ParamRoute(array(
    'key' => 'route_9',
    //'source' => 'route_2:{arg_1}:test_1',
    //*
    'source' => 'class/[class]/arg/{strAdd2}/method/[method]/arg/{strAdd}',
    'element' => [
        'start' => '[',
        'end' => ']',
        //'exclude_value' => ['\\', '/']
    ],
    'argument' => [
        //'exclude_value' => ['\\', '/']
    ],
    //*/
    'call' => [
        'class_path_pattern' => 'liberty_code\\route\\route\\test\\%1$s',
        'method_name_pattern' => '%2$s'
    ]
));

$objRoute1Bis = new ParamRoute(array(
    'key' => 'route_1_bis',
    'source' => 'route_1/arg_1/{strAdd}/arg_2/value arg 2/test_1/',
    'argument' => [
        'exclude_value' => ['\\', '/']
    ],
    'call' => [
        'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest2:action'
    ],
    //'order' => -1
));

$tabRoute = array(
    $objRoute1,
    $objRoute2,
    $objRoute3,
    $objRoute4,
    $objRoute5,
    $objRoute6,
    $objRoute7,
    $objRoute8,
    $objRoute9,
    $objRoute1Bis
);
$objRouteCollection->setTabRoute($tabRoute);

echo('<pre>');print_r($objRouteCollection->beanGetTabData());echo('</pre>');

echo('<br /><br /><br />');



// Test multi match
echo('Test multi match setting: <br />');

echo('Check: <pre>');var_dump($objRouteCollection->checkMultiMatch());echo('</pre>');
$objRouteCollection->setMultiMatch(false);
$objRouteCollection->setMultiMatch(true);
echo('Check after setting: <pre>');var_dump($objRouteCollection->checkMultiMatch());echo('</pre>');

echo('<br /><br /><br />');



// Test check/get from source
$tabSrc = array(
	'route_1/arg_1/value arg 1/arg_2/value arg 2/test_1/', // Ok: Found route_1
	'route_2:value arg 1:test_1', // Ok: Found route_2
    'route_3/test_1/test_2/test_3', // Ok: Found route_3
    'route_4/arg_1/value arg 1/test_1', // Ok: Found route_4
    'route_5/controller/Test2/action/action/', // Ok: Found route_5
    'route_6/key/svc_1/method/action/{value arg 1}/test_1/{value arg 2}', // Ok: Found route_6
    'File/Test1/test2', // Ok: Found route_7
    'FileControllerTest2:runAction:arg:value arg 1:arg:value arg 2', // Ok: Found route_8
    'class/ClassTest1/arg/value arg 1/method/action/arg/value arg 2', // Ok: Found route_9
    'class/ControllerTest2/arg/value arg 1/method/action/arg/value arg 2', // Ok: Found route_9
	'test_1/test_2/test_3', // Ko: Not found
	'test', // Ko: Not found
	3, // Ko: Not found
	15 // Ko: Not found
);

foreach($tabSrc as $strSrc)
{
	echo('Test check, get source "'.$strSrc.'": <br />');
	try{
		$tabRoute = $objRouteCollection->getTabRoute($strSrc, true);
        $tabCallable = $objRouteCollection->getTabCallable($strSrc, true);
		$boolRouteExists = $objRouteCollection->checkMatches($strSrc);
		
		echo('Check: <pre>');var_dump($boolRouteExists);echo('</pre>');
		echo('Count route: <pre>');var_dump(count($tabRoute));echo('</pre>');
        echo('Count callable: <pre>');var_dump(count($tabCallable));echo('</pre>');
		
		if(count($tabRoute) > 0)
		{
			for($intCpt = 0; $intCpt < count($tabRoute); $intCpt++)
			{
				/** @var RouteInterface $objRoute */
				$objRoute = $tabRoute[$intCpt];
				$callable = $objRoute->getCallable($strSrc);

				echo('Get['.$intCpt.']: route key: <pre>');print_r($objRoute->getStrKey());echo('</pre>');
				echo('Get['.$intCpt.']: route match: <pre>');var_dump($objRoute->checkMatches($strSrc));echo('</pre>');
				echo('Call['.$intCpt.']: callable: <pre>');var_dump(is_callable($callable) ? $callable(): $callable);echo('</pre>');
				echo('<br />');
			}
		}
		else
		{
			echo('Get: not found<br />');
		}
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test callable
$tabInfo = array(
    [
        $objRoute1,
        [],
        ['value arg 1 param', 'value arg 2 param']
    ], // Ok

    [
        $objRoute2,
        [],
        []
    ], // Ok

    [
        $objRoute6,
        ['svc_1', 'action'],
        ['route 6 value arg 1 param', 'route 6 value arg 2 param']
    ], // Ok

    [
        $objRoute8,
        ['FileControllerTest2', 'runAction'],
        ['strAdd' => 'value arg 1']
    ] // Ok
);

foreach($tabInfo as $info)
{
    $objRoute = $info[0];
    $tabStrCallElm = $info[1];
    $tabCallArg = $info[2];

    echo('Test ' . $objRoute->getStrKey() . ' callable param: <br />');
    $callable = $objRoute->getObjCall()->getCallable($tabCallArg, $tabStrCallElm);
    echo('Call: callable: <pre>');var_dump(is_callable($callable) ? $callable(): $callable);echo('</pre>');

    echo('<br /><br /><br />');
}



// Test get handle source
$tabInfo = array(
    [
        $objRoute1,
        [],
        ['Build value arg 1_1', 'Build value arg 1_2']
    ], // Ok

    [
        $objRoute2,
        [],
        ['Build value arg 2_1', 'Build value arg 2_2']
    ], // Ok

    [
        $objRoute3,
        [],
        ['Build value arg 3_1', 'Build value arg 3_2']
    ], // Ok

    [
        $objRoute4,
        [],
        []
    ], // Ok:null

    [
        $objRoute5,
        [],
        ['Build value arg 5_1', 'Build value arg 5_2']
    ], // Ok: null

    [
        $objRoute6,
        ['build 6 svc_1', 'build action'],
        ['Build value arg 6_1', 'Build value arg 6_2']
    ], // Ok

    [
        $objRoute7,
        [],
        ['Build value arg 7_1', 'Build value arg 7_2']
    ], // Ok: null

    [
        $objRoute8,
        ['FileControllerTest2', 'runAction'],
        ['Build value arg 8_1', 'Build value arg 8_2']
    ] // Ok
);

foreach($tabInfo as $info)
{
    $objRoute = $info[0];
    $tabStrCallElm = $info[1];
    $tabCallArg = $info[2];

    echo('Test ' . $objRoute->getStrKey() . ' get source param: <br />');
    echo('<pre>');var_dump($objRoute->getStrHandleSource($tabCallArg, $tabStrCallElm));echo('</pre>');

    echo('<br /><br /><br />');
}



// Test remove route
echo('Test remove: <br />');

echo('Before removing: <pre>');print_r($objRouteCollection->getTabKey(true));echo('</pre>');

$objRouteCollection->removeRouteAll();
echo('After removing: <pre>');print_r($objRouteCollection->getTabKey(true));echo('</pre>');

echo('<br /><br /><br />');


