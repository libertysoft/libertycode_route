<?php
/**
 * Description :
 * This class allows to define fix route.
 * Fix route provides routes with fix specific source.
 *
 * Fix route uses the following specified configuration:
 * [
 *     Default route configuration,
 *
 *     source(required): "string source (applied to check route path source)",
 *
 *     element(optional): [
 *          'string element 1',
 *          ...,
 *          'string element N'
 *     ],
 *
 *     argument(optional): [...]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\fix\model;

use liberty_code\route\route\model\DefaultRoute;

use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\fix\library\ConstFixRoute;
use liberty_code\route\route\fix\exception\ConfigInvalidFormatException;



class FixRoute extends DefaultRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoute::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkMatches($strSrc)
    {
		// Init var
		$strSrc = $this->getStrMatchFormatSource($strSrc);
		$result = ($this->getStrSource() == $strSrc);
		
        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get string source.
     *
     * @return string
     */
    protected function getStrSource()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstFixRoute::TAB_CONFIG_KEY_SOURCE];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabStrCallElm($strSrc)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = array();

        // Check argument config provided and valid
        if(isset($tabConfig[ConstFixRoute::TAB_CONFIG_KEY_ELEMENT]))
        {
            // Get arguments array
            $result = $tabConfig[ConstFixRoute::TAB_CONFIG_KEY_ELEMENT];
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabCallArg($strSrc)
    {
        // Init var
		$tabConfig = $this->getTabConfig();
        $result = array();

		// Check argument config provided and valid
        if(isset($tabConfig[ConstFixRoute::TAB_CONFIG_KEY_ARGUMENT]))
        {
            // Get arguments array
            $result = $tabConfig[ConstFixRoute::TAB_CONFIG_KEY_ARGUMENT];
        }

        // Return result
        return $result;
    }
	
	
	
	/**
     * @inheritdoc
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
	{
        // Init var
        $result = parent::getStrHandleSource($tabCallArg, $tabStrCallElm);

        // Get source, if required
        if(is_null($result))
        {
            $result = $this->getStrSource();
        }

        // Return result
        return $result;
    }



}