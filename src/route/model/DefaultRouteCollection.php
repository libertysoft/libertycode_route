<?php
/**
 * Description :
 * This class allows to define default route collection class.
 * key: route key => route.
 * Default route collection uses the following specified configuration:
 * [
 *     multi_match(optional: got false if multi_match not found): true / false
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\route\route\api\RouteCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\route\config\model\DefaultConfig;
use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\route\exception\CollectionConfigInvalidFormatException;
use liberty_code\route\route\exception\CollectionKeyInvalidFormatException;
use liberty_code\route\route\exception\CollectionValueInvalidFormatException;



class DefaultRouteCollection extends DefaultBean implements RouteCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var CallFactoryInterface */
    protected $objCallFactory;



    /** @var array */
    protected $tabConfig;
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param CallFactoryInterface $objCallFactory = null
     * @param array $tabConfig = null
     */
    public function __construct(
        CallFactoryInterface $objCallFactory = null,
        array $tabConfig = null,
        array $tabData = array())
    {
        // Init var
        $this->objCallFactory = null;
        $this->tabConfig = array();

        // Call parent constructor
        parent::__construct($tabData);

        // Init call factory if required
        if(!is_null($objCallFactory))
        {
            $this->setCallFactory($objCallFactory);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var RouteInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkMultiMatch()
    {
        // Init var
        $result = false;
        $tabConfig = $this->getTabConfig();

        // Get from configuration if found
        if(array_key_exists(ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH, $tabConfig))
        {
            $result = (intval($tabConfig[ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjRoute($strKey)));
    }



	/** 
	 * @inheritdoc
	 */
	public function checkMatches($strSrc)
	{
		// Return result
        return (count($this->getTabRoute($strSrc)) > 0);
	}
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get route configuration.
     *
     * @return DefaultConfig
     */
    public function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



    /**
     * @inheritdoc
     */
    public function getObjCallFactory()
    {
        // Return result
        return $this->objCallFactory;
    }



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey($sortAsc = null)
    {
        // Init var
        $result = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);

        // Sort, if required
        if(is_bool($boolSortAsc))
        {
            // Sort
            usort(
                $result,
                function($strKey1, $strKey2)
                {
                    $objRoute1 = $this->getObjRoute($strKey1);
                    $objRoute2 = $this->getObjRoute($strKey2);
                    $result = $objRoute1->getIntSortCompare($objRoute2);

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($result);
                $result = array_values($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjRoute($strKey)
    {
        // Init var
        $result = null;

        // Try to get route object if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);;
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function getTabRoute($strSrc, $sortAsc = null)
	{
		// Init var
		$result = array();
		$boolMultiMatch = $this->checkMultiMatch();

        // Run each route
        $tabKey = $this->getTabKey($sortAsc);
        for($intCpt = 0; ($intCpt < count($tabKey)) && ($boolMultiMatch || (count($result) == 0)); $intCpt++)
        {
            // Get route
            $strKey = $tabKey[$intCpt];
            $objRoute = $this->getObjRoute($strKey);

            // Register route, if required
            if($objRoute->checkMatches($strSrc))
            {
                $result[] = $objRoute;
            }
        }
		
		// Return result
		return $result;
	}



    /**
     * @inheritdoc
     */
    public function getTabCallable($strSrc, $sortAsc = null)
    {
        // Init var
        $result = array();
        $tabRoute = $this->getTabRoute($strSrc, $sortAsc);

        // Run all routes found
        foreach($tabRoute as $objRoute)
        {
            // Register route callback function
            $result[] = $objRoute->getCallable($strSrc);
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setCallFactory(CallFactoryInterface $objCallFactory)
    {
        // Set data
        $this->objCallFactory = $objCallFactory;
    }



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @throws CollectionConfigInvalidFormatException
     */
    public function setConfig(array $tabConfig)
    {
        // Set check argument
        CollectionConfigInvalidFormatException::setCheck($tabConfig);

        $this->tabConfig = $tabConfig;
    }



    /**
     * @inheritdoc
     * @throws CollectionConfigInvalidFormatException
     */
    public function setMultiMatch($boolMultiMatch)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set force access option
        $tabConfig[ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH] = $boolMultiMatch;

        // Set configuration
        $this->setConfig($tabConfig);
    }



	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setRoute(RouteInterface $objRoute)
	{
		// Init var
		$strKey = $objRoute->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objRoute);
		
		// return result
		return $strKey;
	}
	
	
	
	/**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

		// Register route collection on route object
        /** @var RouteInterface $val */
		$val->setRouteCollection($this);
    }



    /**
     * @inheritdoc
     */
    public function setTabRoute($tabRoute)
    {
        // Init var
        $result = array();

        // Case index array of routes
        if(is_array($tabRoute))
        {
            // Run all routes and for each, try to set
            foreach($tabRoute as $route)
            {
                $strKey = $this->setRoute($route);
                $result[] = $strKey;
            }
        }
        // Case collection of routes
        else if($tabRoute instanceof RouteCollectionInterface)
        {
            // Run all routes and for each, try to set
            foreach($tabRoute->getTabKey() as $strKey)
            {
                $objRoute = $tabRoute->getObjRoute($strKey);
                $strKey = $this->setRoute($objRoute);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRoute($strKey)
    {
        // Init var
        $result = $this->getObjRoute($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRouteAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeRoute($strKey);
        }
    }



}