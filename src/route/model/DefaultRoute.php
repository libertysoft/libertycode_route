<?php
/**
 * Description :
 * This class allows to define default route class.
 * Can be consider is base of all route type.
 *
 * Default route uses the following specified configuration:
 * [
 *     key(optional: hash got if not found): "string route key",
 *
 *     call(required):[
 *         Call builder destination configuration (@see CallFactoryInterface::getObjCall() )
 *     ],
 *
 *     build_source_pattern(optional):
 *         "string sprintf pattern,
 *         to build a valid source from specified arguments and elements,
 *         where '%N$s' replaced by first elements items, second arguments items",
 *
 *     order(optional: 0 got if not found): integer,
 *
 *     sort_compare_default(optional: got @see ConstRoute::SORT_COMPARE_EQUAL if not found):
 *         integer sort comparison analysis (@see RouteInterface::getIntSortCompare() result format),
 *
 *     sort_compare_use_key(optional: got false if not found): true / false
 *         Use alphanumeric key sort comparison if true
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\route\route\api\RouteInterface;

use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\route\config\model\DefaultConfig;
use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\api\RouteCollectionInterface;
use liberty_code\route\route\exception\ConfigInvalidFormatException;
use liberty_code\route\route\exception\RouteCollectionInvalidFormatException;



abstract class DefaultRoute extends FixBean implements RouteInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param RouteCollectionInterface $objRouteCollection = null
     */
    public function __construct(array $tabConfig = null, RouteCollectionInterface $objRouteCollection = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init route collection if required
        if(!is_null($objRouteCollection))
        {
            $this->setRouteCollection($objRouteCollection);
        }

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstRoute::DATA_KEY_DEFAULT_ROUTE_COLLECTION))
        {
            $this->__beanTabData[ConstRoute::DATA_KEY_DEFAULT_ROUTE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstRoute::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRoute::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRoute::DATA_KEY_DEFAULT_ROUTE_COLLECTION,
            ConstRoute::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoute::DATA_KEY_DEFAULT_ROUTE_COLLECTION:
                    RouteCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstRoute::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if sort comparison by key option is selected.
     *
     * @return boolean
     */
    protected function checkSortCompareUseKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
        array_key_exists(ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY, $tabConfig) ?
            (intval($tabConfig[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) != 0) :
            false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get route configuration.
     *
     * @return DefaultConfig
     */
    public function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



	/**
     * @inheritdoc
     */
    public function getObjRouteCollection()
    {
        // Return result
        return $this->beanGet(ConstRoute::DATA_KEY_DEFAULT_ROUTE_COLLECTION);
    }



    /**
     * Get call factory object.
     *
     * @return null|CallFactoryInterface
     */
    protected function getObjCallFactory()
    {
        // Init var
        $result = null;
        $objRouteCollection = $this->getObjRouteCollection();

		// Get call factory if found
        if(!is_null($objRouteCollection))
        {
            $result = $objRouteCollection->getObjCallFactory();
        }

        // Return result
        return $result;
    }
	
	

    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstRoute::TAB_CONFIG_KEY_KEY, $tabConfig) ?
                $tabConfig[ConstRoute::TAB_CONFIG_KEY_KEY] :
                static::getStrHash($this)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstRoute::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get integer order,
     * from configuration.
     *
     * @return integer
     */
    public function getIntOrder()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstRoute::TAB_CONFIG_KEY_ORDER, $tabConfig) ?
                $tabConfig[ConstRoute::TAB_CONFIG_KEY_ORDER] :
                0
        );

        // Return result
        return $result;
    }



    /**
     * Get default sort comparison analysis,
     * from configuration.
     *
     * @return integer
     */
    protected function getIntSortCompareDefault()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT, $tabConfig) ?
                $tabConfig[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT] :
                ConstRoute::SORT_COMPARE_EQUAL
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjCall()
    {
        // Init var
        $result = null;
        $objCallFactory = $this->getObjCallFactory();
        $tabConfig = $this->getTabConfig();

        // Get call, if possible
        if(
            array_key_exists(ConstRoute::TAB_CONFIG_KEY_CALL, $tabConfig) &&
            (!is_null($objCallFactory))
        )
        {
            $tabCallConfig = $tabConfig[ConstRoute::TAB_CONFIG_KEY_CALL];
            $result = $objCallFactory->getObjCall($tabCallConfig);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getCallable($strSrc)
    {
        // Init var
        $result = null;
        $objCall = $this->getObjCall();

        // Get callback function, if possible
        if(!is_null($objCall))
        {
            $tabStrElm = $this->getTabStrCallElm($strSrc);
            $tabArg = $this->getTabCallArg($strSrc);

            if(
                (!is_null($tabStrElm)) &&
                (!is_null($tabArg))
            )
            {
                $result = $objCall->getCallable($tabArg, $tabStrElm);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = null;
        $tabConfig = $this->getTabConfig();

        // Get source if possible
        if(isset($tabConfig[ConstRoute::TAB_CONFIG_KEY_BUILD_SOURCE_PATTERN]))
        {
            $tabSrcArg = array_merge(array_values($tabStrCallElm), array_values($tabCallArg));
            $strSrcPattern = $tabConfig[ConstRoute::TAB_CONFIG_KEY_BUILD_SOURCE_PATTERN];
            // Hide potentially warnings to not disturb the process
            $strSrc = @vsprintf($strSrcPattern, $tabSrcArg);

            // Register in result, if source valid
            if(is_string($strSrc))
            {
                $result = $strSrc;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get formatted source,
     * for match validation,
     * from specified source.
     *
     * @param string $strSrc
     * @return string
     */
    protected function getStrMatchFormatSource($strSrc)
    {
        // Return result
        return $strSrc;
    }



    /**
     * Get formatted source,
     * from specified source,
     * to get array of string elements.
     *
     * @param string $strSrc
     * @return string
     */
    protected function getStrCallElmFormatSource($strSrc)
    {
        // Return result
        return $strSrc;
    }



    /**
     * Get formatted source,
     * from specified source,
     * to get array of arguments,
     *
     * @param string $strSrc
     * @return string
     */
    protected function getStrCallArgFormatSource($strSrc)
    {
        // Return result
        return $strSrc;
    }



    /**
     * @inheritdoc
     */
    public function getIntSortCompare(RouteInterface $objRoute)
    {
        // Init var
        $result = ConstRoute::SORT_COMPARE_EQUAL;

        // Get order comparison, if required
        if($objRoute instanceof DefaultRoute)
        {
            $intOrder = $this->getIntOrder();
            $intRouteOrder = $objRoute->getIntOrder();
            $result = (
                ($intOrder > $intRouteOrder) ?
                    ConstRoute::SORT_COMPARE_GREATER :
                    (
                        ($intOrder < $intRouteOrder) ?
                            ConstRoute::SORT_COMPARE_LESS :
                            ConstRoute::SORT_COMPARE_EQUAL
                    )
            );
        }

        // Get alphanumeric key comparison, if required
        if(
            ($result == ConstRoute::SORT_COMPARE_EQUAL) &&
            $this->checkSortCompareUseKey()
        )
        {
            $strKey = $this->getStrKey();
            $strRouteKey = $objRoute->getStrKey();
            $result = (
                ($strKey > $strRouteKey) ?
                    ConstRoute::SORT_COMPARE_GREATER :
                    (
                        ($strKey < $strRouteKey) ?
                            ConstRoute::SORT_COMPARE_LESS :
                            ConstRoute::SORT_COMPARE_EQUAL
                    )
            );
        }

        // Get default comparison, if required
        $result = (
            ($result == ConstRoute::SORT_COMPARE_EQUAL) ?
                $this->getIntSortCompareDefault() :
                $result
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setRouteCollection(RouteCollectionInterface $objRouteCollection = null)
	{
		$this->beanSet(ConstRoute::DATA_KEY_DEFAULT_ROUTE_COLLECTION, $objRouteCollection);
	}



    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstRoute::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }




	
    // Methods statics
    // ******************************************************************************

    /**
     * Get string hash.
     *
     * @param $objRoute
     * @return string
     */
    public static function getStrHash(self $objRoute)
    {
        // Return result
        return ConstRoute::CONF_DEFAULT_ROUTE_HASH_PREFIX . spl_object_hash($objRoute);
    }



}