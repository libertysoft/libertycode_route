<?php
/**
 * Description :
 * This class allows to describe behavior of route factory class.
 * Route factory allows to provide new or specified route instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined route types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\factory\api;

use liberty_code\route\route\api\RouteInterface;



interface RouteFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of route,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrRouteClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance route,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param RouteInterface $objRoute = null
     * @return null|RouteInterface
     */
    public function getObjRoute(
        array $tabConfig,
        $strConfigKey = null,
        RouteInterface $objRoute = null
    );
}