<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/route/factory/test/RouteFactoryTest.php');

// Use
use liberty_code\route\route\model\DefaultRouteCollection;
use liberty_code\route\route\pattern\model\PatternRoute;
use liberty_code\route\route\param\model\ParamRoute;
use liberty_code\route\route\fix\model\FixRoute;
use liberty_code\route\route\separator\model\SeparatorRoute;



// Init var
$tabConfig = array(
    //'multi_match' => 1
);
$objRouteCollection = new DefaultRouteCollection($objCallFactory, $tabConfig);
$objRouteFactory->setObjRouteCollection($objRouteCollection);



// Test new route
$tabRouteData = array(
    [
        [
            'type' => 'pattern',
            'key' => 'route_1',
            'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
            'argument' => 'arg_\d/([^/]+)',
            'call' => [
                'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
                //'class_path_format_require' => true
            ],
            'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/'
        ],
        'route_1'
    ], // Ok

    [
        [
            'argument' => [
                'start' => '[',
                'end' => ']'
            ],
            'call' => [
                'type' => 'dependency',
                'dependency_key_pattern' => 'svc_1:action'
            ]
        ],
        'route_2:[strAdd2]:test_1'
    ], // Ok

    [
        [
            'type' => 'fix',
            'key' => 'route_3',
            'source' => 'route_3/test_1/test_2/test_3',
            'argument' => [
                'argument value 1'
            ],
            'call' => [
                'type' => 'file',
                'file_path_pattern' => 'src/route/test/FileControllerTest1.php',
                'file_path_format_require' => 7
            ]
        ],
        'route_3_not_care'
    ], // Ok

    [
        [
            'type' => 'fix',
            'key' => 'route_4',
            'separator' => '/',
            'argument' => [
                'argument value 1'
            ],
            'call' => [
                'type' => 'file',
                'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
                'file_path_format_require' => true
            ]
        ]
    ], // Ko: separator route config provide for fix route

    [
        [
            'type' => 'separator',
            'key' => 'route_4',
            'separator' => '/',
            'argument' => [
                'argument value 1'
            ],
            'call' => [
                'type' => 'file',
                'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
                'file_path_format_require' => true
            ]
        ]
    ], // Ok

    [
        [
            'type' => 'pattern',
            'key' => 'route_1',
            'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
            'argument' => 'arg_\d/([^/]+)',
            'call' => [
                'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
                //'class_path_format_require' => true
            ],
            'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/'
        ],
        'route_1',
        new PatternRoute()
    ], // Ok

    [
        [
            'type' => 'pattern',
            'key' => 'route_1',
            'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
            'argument' => 'arg_\d/([^/]+)',
            'call' => [
                'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
                //'class_path_format_require' => true
            ],
            'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/'
        ],
        'route_1',
        new ParamRoute()
    ], // Ko: not found: param route used for pattern route config

    [
        [
            'type' => 'param',
            'key' => 'route_2',
            //'source' => 'route_2:{arg_1}:test_1',
            //*
            'argument' => [
                'start' => '[',
                'end' => ']'
            ],
            //*/
            'call' => [
                'type' => 'dependency',
                'dependency_key_pattern' => 'svc_1:action'
            ]
        ],
        'route_2:[strAdd2]:test_1',
        new ParamRoute()
    ], // Ok

    [
        [
            'type' => 'param',
            'key' => 'route_2',
            //'source' => 'route_2:{arg_1}:test_1',
            //*
            'argument' => [
                'start' => '[',
                'end' => ']'
            ],
            //*/
            'call' => [
                'type' => 'dependency',
                'dependency_key_pattern' => 'svc_1:action'
            ]
        ],
        'route_2:[strAdd2]:test_1',
        new FixRoute()
    ], // Ko: not found: fix route used for param route config

    [
        [
            'type' => 'fix',
            'key' => 'route_3',
            'source' => 'route_3/test_1/test_2/test_3',
            'argument' => [
                'argument value 1'
            ],
            'call' => [
                'type' => 'file',
                'file_path_pattern' => 'src/route/test/FileControllerTest1.php',
                'file_path_format_require' => 7
            ]
        ],
        'route_3',
        new FixRoute()
    ], // Ok

    [
        [
            'type' => 'fix',
            'key' => 'route_3',
            'source' => 'route_3/test_1/test_2/test_3',
            'argument' => [
                'argument value 1'
            ],
            'call' => [
                'type' => 'file',
                'file_path_pattern' => 'src/route/test/FileControllerTest1.php',
                'file_path_format_require' => 7
            ]
        ],
        'route_3',
        new SeparatorRoute()
    ], // Ko: not found: separator route used for fix route config

    [
        [
            'type' => 'separator',
            'separator' => '/',
            'argument' => [
                'argument value 1'
            ],
            'call' => [
                'type' => 'file',
                'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
                'file_path_format_require' => true
            ]
        ],
        'route_4',
        new SeparatorRoute()
    ] // Ok
);

foreach($tabRouteData as $routeData)
{
    echo('Test new route: <br />');
    echo('<pre>');var_dump($routeData);echo('</pre>');

    try{
        $tabConfig = $routeData[0];
        $strConfigKey = (isset($routeData[1]) ? $routeData[1] : null);
        $objRoute = (isset($routeData[2]) ? $routeData[2] : null);
        $objRoute = $objRouteFactory->getObjRoute($tabConfig, $strConfigKey, $objRoute);

        echo('Class path: <pre>');var_dump($objRouteFactory->getStrRouteClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objRoute))
        {
            echo('Route class path: <pre>');var_dump(get_class($objRoute));echo('</pre>');
            echo('Route key: <pre>');var_dump($objRoute->getStrKey());echo('</pre>');
            echo('Route config: <pre>');var_dump($objRoute->getTabConfig());echo('</pre>');
            echo('Route call: <pre>');var_dump($objRoute->getObjCall());echo('</pre>');
        }
        else
        {
            echo('Route not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


