<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/route/test/CallFactoryTest.php');

// Use
use liberty_code\route\route\factory\standard\model\StandardRouteFactory;

$objRouteFactory = new StandardRouteFactory();


