<?php
/**
 * Description :
 * This class allows to define standard route factory class.
 * Standard route factory allows to provide and hydrate route instance.
 *
 * Standard route factory uses the following specified configuration, to get and hydrate event:
 * [
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on pattern route configuration)"
 *     type(required): "pattern",
 *     Pattern route configuration (@see PatternRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on param route configuration)"
 *     type(optional): "param",
 *     Param route configuration (@see ParamRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on fix route configuration)"
 *     type(required): "fix",
 *     Fix route configuration (@see FixRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional): "route key"
 *     type(required): "separator",
 *     Separator route configuration (@see SeparatorRoute )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\factory\standard\model;

use liberty_code\route\route\factory\model\DefaultRouteFactory;

use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\pattern\library\ConstPatternRoute;
use liberty_code\route\route\pattern\model\PatternRoute;
use liberty_code\route\route\param\library\ConstParamRoute;
use liberty_code\route\route\param\model\ParamRoute;
use liberty_code\route\route\fix\library\ConstFixRoute;
use liberty_code\route\route\fix\model\FixRoute;
use liberty_code\route\route\separator\model\SeparatorRoute;
use liberty_code\route\route\factory\library\ConstRouteFactory;
use liberty_code\route\route\factory\standard\library\ConstStandardRouteFactory;



class StandardRouteFactory extends DefaultRouteFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Get source configuration, from type
            $strConfigKeySource = null;
            $strConfigType = (
                array_key_exists(ConstRouteFactory::TAB_CONFIG_KEY_TYPE, $tabConfig) ?
                    $tabConfig[ConstRouteFactory::TAB_CONFIG_KEY_TYPE] :
                    null
            );
            switch($strConfigType)
            {
                case ConstStandardRouteFactory::CONFIG_TYPE_PATTERN:
                    $strConfigKeySource = ConstPatternRoute::TAB_CONFIG_KEY_SOURCE;
                    break;

                case null:
                case ConstStandardRouteFactory::CONFIG_TYPE_PARAM:
                    $strConfigKeySource = ConstParamRoute::TAB_CONFIG_KEY_SOURCE;
                    break;

                case ConstStandardRouteFactory::CONFIG_TYPE_FIX:
                    $strConfigKeySource = ConstFixRoute::TAB_CONFIG_KEY_SOURCE;
                    break;
            }

            // Add configured key as source, if required
            if(
                (!is_null($strConfigKeySource)) &&
                (!array_key_exists($strConfigKeySource, $result))
            )
            {
                $result[$strConfigKeySource] = $strConfigKey;
            }
            // Add configured key as route key, if required
            else if(!array_key_exists(ConstRoute::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstRoute::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrRouteClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of route, from type
        switch($strConfigType)
        {
            case ConstStandardRouteFactory::CONFIG_TYPE_PATTERN:
                $result = PatternRoute::class;
                break;

            case null:
            case ConstStandardRouteFactory::CONFIG_TYPE_PARAM:
                $result = ParamRoute::class;
                break;

            case ConstStandardRouteFactory::CONFIG_TYPE_FIX:
                $result = FixRoute::class;
                break;

            case ConstStandardRouteFactory::CONFIG_TYPE_SEPARATOR:
                $result = SeparatorRoute::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}