<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\factory\standard\library;



class ConstStandardRouteFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE_PATTERN = 'pattern';
    const CONFIG_TYPE_PARAM = 'param';
    const CONFIG_TYPE_FIX = 'fix';
    const CONFIG_TYPE_SEPARATOR = 'separator';
}