<?php
/**
 * Description :
 * This class allows to define default route factory class.
 * Can be consider is base of all route factory type.
 *
 * Default route factory uses the following specified configuration, to get and hydrate route:
 * [
 *     type(optional): "string constant to determine route type",
 *
 *     ... specific route configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\route\route\factory\api\RouteFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\route\api\RouteCollectionInterface;
use liberty_code\route\route\exception\RouteCollectionInvalidFormatException;
use liberty_code\route\route\factory\library\ConstRouteFactory;
use liberty_code\route\route\factory\exception\FactoryInvalidFormatException;
use liberty_code\route\route\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|RouteCollectionInterface getObjRouteCollection() Get route collection object.
 * @method void setObjRouteCollection(null|RouteCollectionInterface $objRouteCollection) Set route collection object.
 * @method null|RouteFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|RouteFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultRouteFactory extends DefaultFactory implements RouteFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RouteCollectionInterface $objRouteCollection = null
     * @param RouteFactoryInterface $objFactory = null
     */
    public function __construct(
        RouteCollectionInterface $objRouteCollection = null,
        RouteFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init route collection
        $this->setObjRouteCollection($objRouteCollection);

        // Init route factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRouteFactory::DATA_KEY_DEFAULT_ROUTE_COLLECTION))
        {
            $this->__beanTabData[ConstRouteFactory::DATA_KEY_DEFAULT_ROUTE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstRouteFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstRouteFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRouteFactory::DATA_KEY_DEFAULT_ROUTE_COLLECTION,
            ConstRouteFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRouteFactory::DATA_KEY_DEFAULT_ROUTE_COLLECTION:
                    RouteCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstRouteFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified route.
     * Overwrite it to set specific call hydration.
     *
     * @param RouteInterface $objRoute
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateRoute(RouteInterface $objRoute, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstRouteFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstRouteFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate route
        $objRouteCollection = $this->getObjRouteCollection();
        if(!is_null($objRouteCollection))
        {
            $objRoute->setRouteCollection($objRouteCollection);
        }

        $objRoute->setConfig($tabConfigFormat);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified route object.
     *
     * @param RouteInterface $objRoute
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(RouteInterface $objRoute, array $tabConfigFormat)
    {
        // Init var
        $strRouteClassPath = $this->getStrRouteClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strRouteClassPath)) &&
            ($strRouteClassPath == get_class($objRoute))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstRouteFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstRouteFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of route,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrRouteClassPathFromType($strConfigType);



    /**
     * Get string class path of route engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrRouteClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrRouteClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrRouteClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrRouteClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrRouteClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance route,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|RouteInterface
     */
    protected function getObjRouteNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrRouteClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance route engine.
     *
     * @param array $tabConfigFormat
     * @param RouteInterface $objRoute = null
     * @return null|RouteInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjRouteEngine(
        array $tabConfigFormat,
        RouteInterface $objRoute = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objRoute = (
            is_null($objRoute) ?
                $this->getObjRouteNew($strConfigType) :
                $objRoute
        );

        // Get and hydrate route, if required
        if(
            (!is_null($objRoute)) &&
            $this->checkConfigIsValid($objRoute, $tabConfigFormat)
        )
        {
            $this->hydrateRoute($objRoute, $tabConfigFormat);
            $result = $objRoute;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjRoute(
        array $tabConfig,
        $strConfigKey = null,
        RouteInterface $objRoute = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjRouteEngine($tabConfigFormat, $objRoute);

        // Get route from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjRoute($tabConfig, $strConfigKey, $objRoute);
        }

        // Return result
        return $result;
    }



}