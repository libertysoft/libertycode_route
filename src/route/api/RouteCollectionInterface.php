<?php
/**
 * Description :
 * This class allows to describe behavior of routes collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\api;

use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\route\route\api\RouteInterface;



interface RouteCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

    /**
     * Check if multi match option is selected.
     * This option allows to specify if search include all routes matched (false means include only first route matched).
     *
     * @return boolean
     */
    public function checkMultiMatch();



    /**
     * Check if specified route key is found.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkExists($strKey);

	
	
	/**
	 * Check if specified source matches with at least one route.
	 * 
	 * @param string $strSrc
	 * @return boolean
	 */
	public function checkMatches($strSrc);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get call factory object.
     *
     * @return null|CallFactoryInterface
     */
    public function getObjCallFactory();



    /**
     * Get index array of keys.
     *
     * @param null|boolean $sortAsc = null
     * @return array
     */
    public function getTabKey($sortAsc = null);



    /**
     * Get route from specified key.
     *
     * @param string $strKey
     * @return null|RouteInterface
     */
    public function getObjRoute($strKey);


	
	/**
	 * Get index array of routes from specified source.
	 * 
	 * @param string $strSrc
     * @param null|boolean $sortAsc = null
	 * @return RouteInterface[]
	 */
	public function getTabRoute($strSrc, $sortAsc = null);


	
    /**
     * Get callback function from specified source.
     *
     * @param string $strSrc
     * @param null|boolean $sortAsc = null
     * @return callable[]
     */
    public function getTabCallable($strSrc, $sortAsc = null);

	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set call factory object.
     *
     * @param CallFactoryInterface $objCallFactory
     */
    public function setCallFactory(CallFactoryInterface $objCallFactory);



    /**
     * Set multi match option.
     * This option allows to specify if search include all routes matched (false means include only first route matched).
     *
     * @param boolean $boolMultiMatch
     */
    public function setMultiMatch($boolMultiMatch);



    /**
     * Set route and return its key.
     *
     * @param RouteInterface $objRoute
     * @return string
     */
    public function setRoute(RouteInterface $objRoute);



    /**
     * Set list of routes (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|RouteCollectionInterface $tabRoute
     * @return array
     */
    public function setTabRoute($tabRoute);



    /**
     * Remove route and return its instance.
     *
     * @param string $strKey
     * @return RouteInterface
     */
    public function removeRoute($strKey);



    /**
     * Remove all routes.
     */
    public function removeRouteAll();
}