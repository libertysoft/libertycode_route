<?php
/**
 * Description :
 * This class allows to describe behavior of route class.
 * Route allows to get a call and a callback function,
 * from specified string source and specified configuration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\api;

use liberty_code\call\call\api\CallInterface;
use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\api\RouteCollectionInterface;



interface RouteInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods value
	// ******************************************************************************
	
	/**
	 * Check if specified source matches with this route.
	 * 
	 * @param string $strSrc
	 * @return boolean
	 */
	public function checkMatches($strSrc);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get route collection object.
	 *
	 * @return null|RouteCollectionInterface
	 */
	public function getObjRouteCollection();
	
	
	
	/**
	 * Get string key (considered as route id).
	 *
	 * @return string
	 */
	public function getStrKey();


	
    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get call object.
     *
     * @return null|CallInterface
     */
    public function getObjCall();



    /**
     * Get array of string elements,
     * for call destination customization,
     * from specified source.
     *
     * @param string $strSrc
     * @return null|array
     */
    public function getTabStrCallElm($strSrc);



    /**
     * Get array of arguments,
     * for call destination,
     * from specified source.
     *
     * @param string $strSrc
     * @return null|array
     */
    public function getTabCallArg($strSrc);



    /**
     * Get callback function,
     * from specified source.
     *
	 * @param string $strSrc
     * @return null|callable
     */
    public function getCallable($strSrc);
	
	
	
	/**
     * Get source if possible,
     * from specified array of arguments (for call destination),
     * and specified array of string elements (for call destination customization).
     *
	 * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
     * @return null|string
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );



    /**
     * Get sort comparison analysis,
     * from the specified route.
     * Result format:
     * @see ConstRoute::SORT_COMPARE_LESS if this route less than specified route.
     * @see ConstRoute::SORT_COMPARE_EQUAL if this route equal specified route.
     * @see ConstRoute::SORT_COMPARE_GREATER if this route greater specified route.
     *
     * @param RouteInterface $objRoute
     * @return integer
     */
    public function getIntSortCompare(RouteInterface $objRoute);



	
	
	// Methods setters
	// ******************************************************************************

	/**
	 * Set route collection object.
	 *
	 * @param RouteCollectionInterface $objRouteCollection = null
	 */
	public function setRouteCollection(RouteCollectionInterface $objRouteCollection = null);



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}