<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\exception;

use liberty_code\route\route\library\ConstRoute;



class CollectionConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRoute::EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid multi match option
            (
                (!isset($config[ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH]) ||
                    is_int($config[ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH]) ||
                    (
                        is_string($config[ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH]) &&
                        ctype_digit($config[ConstRoute::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}