<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\exception;

use liberty_code\route\route\library\ConstRoute;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRoute::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key
            (
                (!isset($config[ConstRoute::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstRoute::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstRoute::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid destination configuration
            isset($config[ConstRoute::TAB_CONFIG_KEY_CALL]) &&
            is_array($config[ConstRoute::TAB_CONFIG_KEY_CALL]) &&
            (count($config[ConstRoute::TAB_CONFIG_KEY_CALL]) > 0) &&

            // Check valid build source pattern
            (
                (!isset($config[ConstRoute::TAB_CONFIG_KEY_BUILD_SOURCE_PATTERN])) ||
                (
                    is_string($config[ConstRoute::TAB_CONFIG_KEY_BUILD_SOURCE_PATTERN]) &&
                    (trim($config[ConstRoute::TAB_CONFIG_KEY_BUILD_SOURCE_PATTERN]) != '')
                )
            ) &&

            // Check valid order
            (
                (!isset($config[ConstRoute::TAB_CONFIG_KEY_ORDER])) ||
                is_int($config[ConstRoute::TAB_CONFIG_KEY_ORDER])
            ) &&

            // Check valid default sort comparison analysis
            (
                (!isset($config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT])) ||
                (
                    is_int($config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT]) &&
                    in_array(
                        $config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT],
                        ConstRoute::getTabSortCompare()
                    )
                )
            ) &&

            // Check valid sort comparison by key option
            (
                (!isset($config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) ||
                    is_int($config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) ||
                    (
                        is_string($config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) &&
                        ctype_digit($config[ConstRoute::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}