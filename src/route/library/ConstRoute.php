<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\library;



class ConstRoute
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_ROUTE_COLLECTION = 'objRouteCollection';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
	
	
	
	// Configuration
    const CONF_DEFAULT_ROUTE_HASH_PREFIX = 'default_route_';

    // Route destination configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_CALL = 'call';
    const TAB_CONFIG_KEY_BUILD_SOURCE_PATTERN = 'build_source_pattern';
    const TAB_CONFIG_KEY_ORDER = 'order';
    const TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT = 'sort_compare_default';
    const TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY = 'sort_compare_use_key';

    // Sort comparison value configuration
    const SORT_COMPARE_LESS = -1;
    const SORT_COMPARE_EQUAL = 0;
    const SORT_COMPARE_GREATER = 1;

    // Collection configuration keys
    const TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH = 'multi_match';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_ROUTE_COLLECTION_INVALID_FORMAT = 'Following route collection "%1$s" invalid! It must be a route collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default route configuration standard.';
	const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default route collection configuration standard.';
	const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a route object in collection.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of sort comparison values.
     *
     * @return array
     */
    static public function getTabSortCompare()
    {
        // Init var
        $result = array(
            self::SORT_COMPARE_LESS,
            self::SORT_COMPARE_EQUAL,
            self::SORT_COMPARE_GREATER
        );

        // Return result
        return $result;
    }



}