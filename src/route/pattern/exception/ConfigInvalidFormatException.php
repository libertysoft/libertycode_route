<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\pattern\exception;

use liberty_code\route\route\pattern\library\ConstPatternRoute;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPatternRoute::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}





	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid source
            isset($config[ConstPatternRoute::TAB_CONFIG_KEY_SOURCE]) &&
            is_string($config[ConstPatternRoute::TAB_CONFIG_KEY_SOURCE]) &&
            (trim($config[ConstPatternRoute::TAB_CONFIG_KEY_SOURCE]) != '') &&

            // Check valid element
            (
                (!isset($config[ConstPatternRoute::TAB_CONFIG_KEY_ELEMENT])) ||
                (
                    is_string($config[ConstPatternRoute::TAB_CONFIG_KEY_ELEMENT]) &&
                    (trim($config[ConstPatternRoute::TAB_CONFIG_KEY_ELEMENT]) != '')
                ) ||
                is_array($config[ConstPatternRoute::TAB_CONFIG_KEY_ELEMENT])
            ) &&

            // Check valid argument
            (
                (!isset($config[ConstPatternRoute::TAB_CONFIG_KEY_ARGUMENT])) ||
                (
                    is_string($config[ConstPatternRoute::TAB_CONFIG_KEY_ARGUMENT]) &&
                    (trim($config[ConstPatternRoute::TAB_CONFIG_KEY_ARGUMENT]) != '')
                ) ||
                is_array($config[ConstPatternRoute::TAB_CONFIG_KEY_ARGUMENT])
            ) &&

            // Check valid option decoration
            (
                (!isset($config[ConstPatternRoute::TAB_CONFIG_KEY_OPTION]
                    [ConstPatternRoute::TAB_CONFIG_KEY_OPTION_DECORATION])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstPatternRoute::TAB_CONFIG_KEY_OPTION]
                        [ConstPatternRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) ||
                    is_int($config[ConstPatternRoute::TAB_CONFIG_KEY_OPTION]
                        [ConstPatternRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) ||
                    (
                        is_string($config[ConstPatternRoute::TAB_CONFIG_KEY_OPTION]
                            [ConstPatternRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) &&
                        ctype_digit($config[ConstPatternRoute::TAB_CONFIG_KEY_OPTION]
                            [ConstPatternRoute::TAB_CONFIG_KEY_OPTION_DECORATION])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}