<?php
/**
 * Description :
 * This class allows to define pattern route.
 * Pattern route provides routes with flexible source, using REGEXPs,
 * to check source match,
 * to get arguments and elements from source, to customize destination.
 *
 * Pattern route uses the following specified configuration:
 * [
 *     Default route configuration,
 *
 *     source(required): "string REGEXP pattern source (applied to check route path source)",
 *
 *     element(optional: got source if not found and argument provided):
 *         "string REGEXP pattern (applied on source to get elements list)",
 *     
 *     OR
 *
 *     element(optional): [
 *          'string element 1',
 *          ...,
 *          'string element N'
 *     ],
 *     
 *     argument(optional: got source if not found):
 *         "string REGEXP pattern (applied on source to get arguments list)",
 *
 *     OR
 *
 *     argument(optional): [...],
 *
 *     option(optional): [
 *         decoration: true / false
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\pattern\model;

use liberty_code\route\route\model\DefaultRoute;

use liberty_code\library\regexp\library\ToolBoxRegexp;
use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\pattern\library\ConstPatternRoute;
use liberty_code\route\route\pattern\exception\ConfigInvalidFormatException;



class PatternRoute extends DefaultRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoute::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods check
	// ******************************************************************************

    /**
     * Check if decoration required.
     *
     * @return boolean
     */
    protected function checkDecorationRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getObjConfig()->checkPatternDecorationIsRequired();

        // Check decoration required
        if(isset($tabConfig[ConstPatternRoute::TAB_CONFIG_KEY_OPTION][ConstPatternRoute::TAB_CONFIG_KEY_OPTION_DECORATION]))
        {
            $result = (intval($tabConfig[ConstPatternRoute::TAB_CONFIG_KEY_OPTION]
                [ConstPatternRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkMatches($strSrc)
    {
        // Init var
        $strSourcePattern = $this->getStrSourcePattern();
        $strSrc = $this->getStrMatchFormatSource($strSrc);
        $result = (preg_match($strSourcePattern, $strSrc) == 1);

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get string pattern with decoration if required.
     *
     * @param string $strPattern
     * @return string
     */
    protected function getStrPatternDecoration($strPattern)
    {
        // Init var
        $result = $strPattern;
        $strPatternDecoration = $this->getObjConfig()->getStrPatternDecoration();
		
        // Set decoration if required
        if($this->checkDecorationRequired() && (!is_null($strPatternDecoration)))
        {
            $result = sprintf($strPatternDecoration, $strPattern);
        }

        // Return result
        return $result;
    }



    /**
     * Get string source pattern.
     *
     * @return null|string
     */
    protected function getStrSourcePattern()
    {
        // Init var
		$tabConfig = $this->getTabConfig();
		$result = $this->getStrPatternDecoration($tabConfig[ConstPatternRoute::TAB_CONFIG_KEY_SOURCE]);
		
        // Return result
        return $result;
    }


	
	/**
	 * Get index array of items (arguments or elements for call destination),
     * from configuration.
     * 
	 * @param string $strSrc
	 * @param string $strDefault
	 * @param string $strConfigKey
     * @return array
     */
    protected function getTabCallItem($strSrc, $strDefault, $strConfigKey)
    {
        // Init var
        $result = array();
		$tabConfig = $this->getTabConfig();

		// Init source item
		$srcItm = $strDefault;
		if(isset($tabConfig[$strConfigKey]))
		{
			$srcItm = $tabConfig[$strConfigKey];
			if(is_string($srcItm))
			{
				$srcItm = $this->getStrPatternDecoration($srcItm);
			}
		}
		
		// Case source item is pattern
		if(is_string($srcItm) && (trim($srcItm) != ''))
		{
			$result = ToolBoxRegexp::getTabMatchValue($strSrc, $srcItm);
		}
		// Case source item is array
		else if(is_array($srcItm))
		{
			$result = $srcItm;
		}

        // Return result
        return $result;
    }
	
	
	
	/**
     * @inheritdoc
     */
    public function getTabStrCallElm($strSrc)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (
        isset($tabConfig[ConstPatternRoute::TAB_CONFIG_KEY_ARGUMENT]) ?
            $this->getStrSourcePattern() :
            ''
        );
        $strSrc = $this->getStrCallElmFormatSource($strSrc);
        $result = $this->getTabCallItem(
            $strSrc,
            $strDefault,
            ConstPatternRoute::TAB_CONFIG_KEY_ELEMENT
        );

        // Return result
        return $result;
    }
	
	
	
    /**
     * @inheritdoc
     */
    public function getTabCallArg($strSrc)
    {
        // Init var
        $strDefault = $this->getStrSourcePattern();
        $strSrc = $this->getStrCallArgFormatSource($strSrc);
        $result = $this->getTabCallItem(
            $strSrc,
            $strDefault,
            ConstPatternRoute::TAB_CONFIG_KEY_ARGUMENT
        );

        // Return result
        return $result;
    }



}