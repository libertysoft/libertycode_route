<?php
/**
 * Description :
 * This class allows to define separator route.
 * Separator route provides routes with flexible source, using separator to get source elements.
 *
 * Separator route uses the following specified configuration:
 * [
 *     Default route configuration,
 *
 *     separator(required): "string route path source separator",
 *
 *     argument(optional):
 *         "string REGEXP pattern (applied on source to get arguments list)",
 *
 *     OR
 *
 *     argument(optional): [...],
 *
 *     option(optional): [
 *         decoration: true / false
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\separator\model;

use liberty_code\route\route\model\DefaultRoute;

use Exception;
use liberty_code\library\regexp\library\ToolBoxRegexp;
use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\separator\library\ConstSeparatorRoute;
use liberty_code\route\route\separator\exception\ConfigInvalidFormatException;



class SeparatorRoute extends DefaultRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoute::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods check
	// ******************************************************************************

    /**
     * Check if decoration required.
     *
     * @return boolean
     */
    protected function checkDecorationRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getObjConfig()->checkPatternDecorationIsRequired();

        // Check decoration required
        if(isset($tabConfig[ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION][ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION_DECORATION]))
        {
            $result = (intval($tabConfig[ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION]
                [ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkMatches($strSrc)
    {
        // Init var
        $strSrc = $this->getStrMatchFormatSource($strSrc);

        try
        {
            $result = (!is_null($this->getCallable($strSrc)));
        }
        catch (Exception $e)
        {
            $result = false;
        }

        // Return result
        return $result;
    }



	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get string pattern with decoration if required.
     *
     * @param string $strPattern
     * @return string
     */
    protected function getStrPatternDecoration($strPattern)
    {
        // Init var
        $result = $strPattern;
        $strPatternDecoration = $this->getObjConfig()->getStrPatternDecoration();

        // Set decoration if required
        if($this->checkDecorationRequired() && (!is_null($strPatternDecoration)))
        {
            $result = sprintf($strPatternDecoration, $strPattern);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabStrCallElm($strSrc)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strSrc = $this->getStrCallElmFormatSource($strSrc);
        $result = explode($tabConfig[ConstSeparatorRoute::TAB_CONFIG_KEY_SEPARATOR], $strSrc);

        // Return result
        return $result;
    }
	
	
	
    /**
     * @inheritdoc
     */
    public function getTabCallArg($strSrc)
    {
		// Init var
        $result = array();
		$tabConfig = $this->getTabConfig();
        $strSrc = $this->getStrCallArgFormatSource($strSrc);

		// Init source argument
		$srcArg = '';
		if(isset($tabConfig[ConstSeparatorRoute::TAB_CONFIG_KEY_ARGUMENT]))
		{
			$srcArg = $tabConfig[ConstSeparatorRoute::TAB_CONFIG_KEY_ARGUMENT];
			if(is_string($srcArg))
			{
				$srcArg = $this->getStrPatternDecoration($srcArg);
			}
		}
		
		// Case source argument is pattern
		if(is_string($srcArg) && (trim($srcArg) != ''))
		{
			$result = ToolBoxRegexp::getTabMatchValue($strSrc, $srcArg);
		}
		// Case source argument is array
		else if(is_array($srcArg))
		{
			$result = $srcArg;
		}

        // Return result
        return $result;
    }



}