<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\separator\library;



class ConstSeparatorRoute
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	const TAB_CONFIG_KEY_SEPARATOR = 'separator';
    const TAB_CONFIG_KEY_ARGUMENT = 'argument';
    const TAB_CONFIG_KEY_OPTION = 'option';
    const TAB_CONFIG_KEY_OPTION_DECORATION = 'decoration';

    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the separator route configuration standard.';
}