<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\separator\exception;

use liberty_code\route\route\separator\library\ConstSeparatorRoute;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstSeparatorRoute::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}





	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid source
            isset($config[ConstSeparatorRoute::TAB_CONFIG_KEY_SEPARATOR]) &&
            is_string($config[ConstSeparatorRoute::TAB_CONFIG_KEY_SEPARATOR]) &&
            (trim($config[ConstSeparatorRoute::TAB_CONFIG_KEY_SEPARATOR]) != '') &&

            // Check valid argument
            (
                (!isset($config[ConstSeparatorRoute::TAB_CONFIG_KEY_ARGUMENT])) ||
                (
                    is_string($config[ConstSeparatorRoute::TAB_CONFIG_KEY_ARGUMENT]) &&
                    (trim($config[ConstSeparatorRoute::TAB_CONFIG_KEY_ARGUMENT]) != '')
                ) ||
                is_array($config[ConstSeparatorRoute::TAB_CONFIG_KEY_ARGUMENT])
            ) &&

            // Check valid option decoration
            (
                (!isset($config[ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION]
                    [ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION_DECORATION])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION]
                    [ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) ||
                    is_int($config[ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION]
                    [ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) ||
                    (
                        is_string($config[ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION]
                        [ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION_DECORATION]) &&
                        ctype_digit($config[ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION]
                        [ConstSeparatorRoute::TAB_CONFIG_KEY_OPTION_DECORATION])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}