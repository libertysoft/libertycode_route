<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\param\exception;

use liberty_code\route\route\param\library\ConstParamRoute;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstParamRoute::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}





	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid source
            isset($config[ConstParamRoute::TAB_CONFIG_KEY_SOURCE]) &&
            is_string($config[ConstParamRoute::TAB_CONFIG_KEY_SOURCE]) &&
            (trim($config[ConstParamRoute::TAB_CONFIG_KEY_SOURCE]) != '') &&

            // Check valid element start
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_START])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_START]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_START]) != '')
                )
            ) &&

            // Check valid element end
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_END])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_END]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_END]) != '')
                )
            ) &&

            // Check valid element include value
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE]) != '')
                ) ||
                (
                    is_array($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE]) &&
                    $checkTabStrIsValid($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE])
                )
            ) &&

            // Check valid element exclude value
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE]) != '')
                ) ||
                (
                    is_array($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE]) &&
                    $checkTabStrIsValid($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE])
                )
            ) &&

            // Check valid argument start
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_START])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_START]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_START]) != '')
                )
            ) &&

            // Check valid argument end
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_END])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_END]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_END]) != '')
                )
            ) &&

            // Check valid argument exclude value
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE]) != '')
                ) ||
                (
                    is_array($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE]) &&
                    $checkTabStrIsValid($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE])
                )
            ) &&

            // Check valid argument exclude value
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE])) ||
                (
                    is_string($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE]) &&
                    (trim($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE]) != '')
                ) ||
                (
                    is_array($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE]) &&
                    $checkTabStrIsValid($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE])
                )
            ) &&

            // Check minimum start requirement
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_START])) ||
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_START])) ||
                (
                    $config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_START] !=
                    $config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                        [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_START]
                )
            ) &&

            // Check minimum end requirement
            (
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_END])) ||
                (!isset($config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_END])) ||
                (
                    $config[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_END] !=
                    $config[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                    [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_END]
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}