<?php
/**
 * Description :
 * This class allows to define param route.
 * Param route provides routes with semi-flexible source, including elements and arguments.
 *
 * Param route uses the following specified configuration:
 * [
 *     Default route configuration,
 *
 *     source(required):
 *         "string formatted source (applied to check route path source),
 *         ex: ...[element 1]...{argument 1}...[element N]...{argument N}...",
 *
 *     element(optional):[
 *         start(optional: got @see DefaultRoute::getObjConfig()::getStrParamArgumentStart() if not found):
 *             "string element start",
 *         end(optional: got @see DefaultRoute::getObjConfig()::getStrParamArgumentStart() if not found):
 *             "string element end",
 *         include_value(optional: got [] if not found):
 *             "string value" OR ["string value 1", ... "string value N"],
 *         exclude_value(optional: got [] if not found):
 *             "string value" OR ["string value 1", ... "string value N"]
 *     ],
 *
 *     argument(optional):[
 *         start(optional: got @see DefaultRoute::getObjConfig()::getStrParamArgumentStart() if not found):
 *             "string argument start",
 *         end(optional: got @see DefaultRoute::getObjConfig()::getStrParamArgumentStart() if not found):
 *             "string argument end",
 *         include_value(optional: got [] if not found):
 *             "string value" OR ["string value 1", ... "string value N"],
 *         exclude_value(optional: got [] if not found):
 *             "string value" OR ["string value 1", ... "string value N"]
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\param\model;

use liberty_code\route\route\model\DefaultRoute;

use liberty_code\library\regexp\library\ToolBoxRegexp;
use liberty_code\route\config\library\ConstConfig;
use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\param\library\ConstParamRoute;
use liberty_code\route\route\param\exception\ConfigInvalidFormatException;



class ParamRoute extends DefaultRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoute::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkMatches($strSrc)
    {
        // Init var
		$strSrc = $this->getStrMatchFormatSource($strSrc);
        $result = (
            (preg_match($this->getStrSourcePattern(), $strSrc) == 1) &&
            (!is_null($this->getTabStrCallElm($strSrc))) &&
            (!is_null($this->getTabCallArg($strSrc)))
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified string elements (for call destination customization),
     * are scoped on specific configuration.
     *
     * @param array $tabStrCallElm
     * @return boolean
     */
    protected function checkStrCallElmIsScoped(array $tabStrCallElm)
    {
        // Init var
        $result = true;
        $tabStrCallElm = array_values($tabStrCallElm);

        // Run each string elements
        $tabElmIncludeValue = $this->getTabElmIncludeValue();
        $tabElmExcludeValue = $this->getTabElmExcludeValue();
        for($cpt = 0; ($cpt < count($tabStrCallElm)) && $result; $cpt++)
        {
            $strElm = $tabStrCallElm[$cpt];

            // Check if element is on include scope
            for($cpt2 = 0; ($cpt2 < count($tabElmIncludeValue)) && $result; $cpt2++)
            {
                $strElmValue = $tabElmIncludeValue[$cpt2];

                $result = (strpos($strElm, $strElmValue) !== false);
            }

            // Check if element is not on exclude scope
            for($cpt2 = 0; ($cpt2 < count($tabElmExcludeValue)) && $result; $cpt2++)
            {
                $strElmValue = $tabElmExcludeValue[$cpt2];

                $result = (strpos($strElm, $strElmValue) === false);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified arguments (for call destination),
     * are scoped on specific configuration.
     *
     * @param array $tabCallArg
     * @return boolean
     */
    protected function checkCallArgIsScoped(array $tabCallArg)
    {
        // Init var
        $result = true;
        $tabCallArg = array_values($tabCallArg);

        // Check argument scope, if required
        $tabArgIncludeValue = $this->getTabArgIncludeValue();
        $tabArgExcludeValue = $this->getTabArgExcludeValue();
        for($cpt = 0; ($cpt < count($tabCallArg)) && $result; $cpt++)
        {
            $arg = $tabCallArg[$cpt];
            $result = (!in_array($arg, $tabArgExcludeValue));

            // Check if argument is on include scope
            for($cpt2 = 0; ($cpt2 < count($tabArgIncludeValue)) && $result; $cpt2++)
            {
                $strArgValue = $tabArgIncludeValue[$cpt2];

                $result = (strpos($arg, $strArgValue) !== false);
            }

            // Check if argument is not on exclude scope
            for($cpt2 = 0; ($cpt2 < count($tabArgExcludeValue)) && $result; $cpt2++)
            {
                $strArgValue = $tabArgExcludeValue[$cpt2];

                $result = (strpos($arg, $strArgValue) === false);
            }
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get string pattern with decoration if required.
     *
     * @param string $strPattern
     * @return string
     */
    protected function getStrPatternDecoration($strPattern)
    {
        // Init var
        $result = $strPattern;
        $strPatternDecoration = ConstConfig::DATA_DEFAULT_VALUE_PATTERN_DECORATION;

        // Set decoration if required
        if(!is_null($strPatternDecoration))
        {
            $result = sprintf($strPatternDecoration, $strPattern);
        }

        // Return result
        return $result;
    }



    /**
     * Get string element start.
     *
     * @return null|string
     */
    protected function getStrElmStart()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getObjConfig()->getStrParamElementStart();

        // Get configured element start, if required
        if(isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT][ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_START]))
        {
            $result = $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT][ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_START];
        }

        // Return result
        return $result;
    }



    /**
     * Get string element end.
     *
     * @return null|string
     */
    protected function getStrElmEnd()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getObjConfig()->getStrParamElementEnd();

        // Get configured element end, if required
        if(isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT][ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_END]))
        {
            $result = $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT][ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_END];
        }

        // Return result
        return $result;
    }



    /**
     * Get string argument start.
     *
     * @return null|string
     */
    protected function getStrArgStart()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getObjConfig()->getStrParamArgumentStart();

        // Get configured argument start, if required
        if(isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT][ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_START]))
        {
            $result = $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT][ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_START];
        }

        // Return result
        return $result;
    }



    /**
     * Get string argument end.
     *
     * @return null|string
     */
    protected function getStrArgEnd()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getObjConfig()->getStrParamArgumentEnd();

        // Get configured argument end, if required
        if(isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT][ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_END]))
        {
            $result = $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT][ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_END];
        }

        // Return result
        return $result;
    }



    /**
     * Get string element pattern to get key.
     *
     * @param boolean $boolSelectIn = false
     * @param boolean $boolDecorate = true
     * @return null|string
     */
    protected function getStrElmPatternKey($boolSelectIn = false, $boolDecorate = true)
    {
        // Init var
        $result = null;
        $strStart = $this->getStrElmStart();
        $strEnd = $this->getStrElmEnd();
        $strPattern = ($boolSelectIn ? '(%1$s[^%2$s]+%2$s)' : '%1$s([^%2$s]+)%2$s');

        // Check start and end provided
        if((!is_null($strStart)) && (!is_null($strEnd)))
        {
            $result = sprintf(
                $strPattern ,
                preg_quote($strStart),
                preg_quote($strEnd)
            );

            // Set decoration if required
            if($boolDecorate)
            {
                $result = $this->getStrPatternDecoration($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string argument pattern to get key.
     *
     * @param boolean $boolSelectIn = false
     * @param boolean $boolDecorate = true
     * @return null|string
     */
    protected function getStrArgPatternKey($boolSelectIn = false, $boolDecorate = true)
    {
        // Init var
        $result = null;
        $strStart = $this->getStrArgStart();
        $strEnd = $this->getStrArgEnd();
        $strPattern = ($boolSelectIn ? '(%1$s[^%2$s]+%2$s)' : '%1$s([^%2$s]+)%2$s');

        // Check start and end provided
        if((!is_null($strStart)) && (!is_null($strEnd)))
        {
            $result = sprintf(
                $strPattern,
                preg_quote($strStart),
                preg_quote($strEnd)
            );

            // Set decoration if required
            if ($boolDecorate) {
                $result = $this->getStrPatternDecoration($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string source pattern.
     *
     * @param boolean $boolElmSelectIn = false
     * @param boolean $boolArgSelectIn = false
     * @return string
     */
    protected function getStrSourcePattern($boolElmSelectIn = false, $boolArgSelectIn = false)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strSrcPattern = $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_SOURCE];
        $boolElmSelectIn = (is_bool($boolElmSelectIn) ? $boolElmSelectIn : false);
        $boolArgSelectIn = (is_bool($boolArgSelectIn) ? $boolArgSelectIn : false);
        $strPatternItemIn = '(.*)';
        $strPatternItem = '.*';

        // Format source pattern with elements, if required
        $strElmPatternKey = $this->getStrElmPatternKey(false, true);
        if(!is_null($strElmPatternKey))
        {
            $strSrcPattern = preg_replace_callback
            (
                $strElmPatternKey,
                // For all pattern key found, replace by pattern select value
                function () use ($boolElmSelectIn, $strPatternItemIn, $strPatternItem)
                {
                    return ($boolElmSelectIn ? $strPatternItemIn : $strPatternItem);
                },
                $strSrcPattern
            );
        }

        // Format source pattern with arguments, if required
        $strArgPatternKey = $this->getStrArgPatternKey(false, true);
        if(!is_null($strArgPatternKey))
        {
            $strSrcPattern = preg_replace_callback
            (
                $strArgPatternKey,
                // For all pattern key found, replace by pattern select value
                function () use ($boolArgSelectIn, $strPatternItemIn, $strPatternItem)
                {
                    return ($boolArgSelectIn ? $strPatternItemIn : $strPatternItem);
                },
                $strSrcPattern
            );
        }

        // Escape source pattern
        $tabSrcPattern = explode($strPatternItemIn, $strSrcPattern);
        $tabSrcPattern = array_map(
            function($strValue) use ($strPatternItem)
            {
                $tabSrcPattern = explode($strPatternItem, $strValue);
                $tabSrcPattern = array_map(
                    function($strValue) {return preg_quote($strValue);},
                    $tabSrcPattern
                );

                return implode($strPatternItem, $tabSrcPattern);
            },
            $tabSrcPattern
        );
        $strSrcPattern = implode($strPatternItemIn, $tabSrcPattern);
		
		// Set result
        $result = $this->getStrPatternDecoration('^' . $strSrcPattern . '$');

        // Return result
        return $result;
    }



    /**
     * Get index array of string element include values.
     *
     * @return array
     */
    protected function getTabElmIncludeValue()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
        (isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE])) ?
            // Get configured element include values, if required
            (
            is_array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE]) ?
                array_values($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE]) :
                array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE])
            ) :
            array()
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of string element exclude values.
     *
     * @return array
     */
    protected function getTabElmExcludeValue()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
        (isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE])) ?
            // Get configured element exclude values, if required
            (
            is_array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE]) ?
                array_values($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE]) :
                array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ELEMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE])
            ) :
            array()
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of string argument include values.
     *
     * @return array
     */
    protected function getTabArgIncludeValue()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
        (isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE])) ?
            // Get configured argument exclude values, if required
            (
            is_array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE]) ?
                array_values($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE]) :
                array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE])
            ) :
            array()
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of string argument exclude values.
     *
     * @return array
     */
    protected function getTabArgExcludeValue()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
        (isset($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE])) ?
            // Get configured argument exclude values, if required
            (
            is_array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
            [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE]) ?
                array_values($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE]) :
                array($tabConfig[ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT]
                [ConstParamRoute::TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE])
            ) :
            array()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabStrCallElm($strSrc)
    {
        // Init var
        $result = array();
        $tabConfig = $this->getTabConfig();
        $strPatternKey = $this->getStrElmPatternKey(true, true);
        $strSrc = $this->getStrCallElmFormatSource($strSrc);

        // Check pattern key found
        if(!is_null($strPatternKey))
        {
            // Get matched values
            $tabKey = ToolBoxRegexp::getTabMatchValue(
                $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_SOURCE],
                $strPatternKey
            );
            $tabValue = ToolBoxRegexp::getTabMatchValue(
                $strSrc,
                $this->getStrSourcePattern(true, false)
            );

            // Check argument found
            if(count($tabKey) == count($tabValue))
            {
                $result = array_combine($tabKey, $tabValue);

                // Check scope
                $result = ($this->checkStrCallElmIsScoped($result) ? $result : null);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabCallArg($strSrc)
    {
        // Init var
        $result = array();
        $tabConfig = $this->getTabConfig();
        $strPatternKey = $this->getStrArgPatternKey(false, true);
        $strSrc = $this->getStrCallArgFormatSource($strSrc);

        // Check pattern key found
        if(!is_null($strPatternKey))
        {
            // Get matched values
            $tabKey = ToolBoxRegexp::getTabMatchValue(
                $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_SOURCE],
                $strPatternKey
            );
            $tabValue = ToolBoxRegexp::getTabMatchValue(
                $strSrc,
                $this->getStrSourcePattern(false, true)
            );

            // Check argument found
            if(count($tabKey) == count($tabValue))
            {
                $result = array_combine($tabKey, $tabValue);

                // Check scope
                $result = ($this->checkCallArgIsScoped($result) ? $result : null);
            }
        }

        // Return result
        return $result;
    }
	
	
	
	/**
	 * @inheritdoc
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
	{
		// Init var
		$result = parent::getStrHandleSource($tabCallArg, $tabStrCallElm);
        
		// Get source, if required
		if(is_null($result))
        {
            $tabConfig = $this->getTabConfig();
            $strSrc = $tabConfig[ConstParamRoute::TAB_CONFIG_KEY_SOURCE];
            $strElmPatternKey = $this->getStrElmPatternKey(false, true);
            $strArgPatternKey = $this->getStrArgPatternKey(false, true);

            // Get source with elements, if required
            if((count($tabStrCallElm) > 0) && (!is_null($strElmPatternKey)))
            {
                $intCpt = 0;
                $strSrc = preg_replace_callback
                (
                    $strElmPatternKey,
                    // For all pattern key found, replace by parameter value
                    function () use ($tabStrCallElm, &$intCpt)
                    {
                        // Init var
                        $result = strval($tabStrCallElm[$intCpt]);
                        $intCpt++;

                        // Return result
                        return $result;
                    },
                    $strSrc
                );
            }

            // Get source with arguments, if required
            if((count($tabCallArg) > 0) && (!is_null($strArgPatternKey)))
            {
                $intCpt = 0;
                $strSrc = preg_replace_callback
                (
                    $strArgPatternKey,
                    // For all pattern key found, replace by parameter value
                    function () use ($tabCallArg, &$intCpt)
                    {
                        // Init var
                        $result = strval($tabCallArg[$intCpt]);
                        $intCpt++;

                        // Return result
                        return $result;
                    },
                    $strSrc
                );
            }

            // Register in result, if source valid
            if(!is_null($strSrc))
            {
                $result = $strSrc;
            }
        }
		
        // Return result
        return $result;
    }



}