<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\route\param\library;



class ConstParamRoute
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	const TAB_CONFIG_KEY_SOURCE = 'source';
    const TAB_CONFIG_KEY_ELEMENT = 'element';
    const TAB_CONFIG_KEY_ELEMENT_START = 'start';
    const TAB_CONFIG_KEY_ELEMENT_END = 'end';
    const TAB_CONFIG_KEY_ELEMENT_INCLUDE_VALUE = 'include_value';
    const TAB_CONFIG_KEY_ELEMENT_EXCLUDE_VALUE = 'exclude_value';
    const TAB_CONFIG_KEY_ARGUMENT = 'argument';
    const TAB_CONFIG_KEY_ARGUMENT_START = 'start';
    const TAB_CONFIG_KEY_ARGUMENT_END = 'end';
    const TAB_CONFIG_KEY_ARGUMENT_INCLUDE_VALUE = 'include_value';
    const TAB_CONFIG_KEY_ARGUMENT_EXCLUDE_VALUE = 'exclude_value';

    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the parameters route configuration standard.';
}