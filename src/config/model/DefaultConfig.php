<?php
/**
 * Description :
 * This class allows to provide configuration for default route tools (model and collection).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\config\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\route\config\library\ConstConfig;
use liberty_code\route\config\exception\PatternDecorationRequireInvalidFormatException;
use liberty_code\route\config\exception\PatternDecorationInvalidFormatException;
use liberty_code\route\config\exception\ParamStartInvalidFormatException;
use liberty_code\route\config\exception\ParamEndInvalidFormatException;



/**
 * @method null|string getStrPatternDecoration() Get default decoration for pattern routes, requiring decoration.
 * @method null|string getStrParamElementStart() Get default element start for parameter routes.
 * @method null|string getStrParamElementEnd() Get default element end for parameter routes.
 * @method null|string getStrParamArgumentStart() Get default argument start for parameter routes.
 * @method null|string getStrParamArgumentEnd() Get default argument end for parameter routes.
 * @method void setBoolPatternDecorationRequire(bool $boolPatternDecorationRequire) Set pattern decoration require option. This option allows to specify if by default a pattern route sources need to be decorated.
 * @method void setStrPatternDecoration(string $strPatternDecoration) Set default decoration for pattern route sources, requiring decoration.
 * @method void setStrParamElementStart(null|string $strParamElementStart) Set default element start for parameter routes.
 * @method void setStrParamElementEnd(null|string $strParamElementEnd) Set default element end for parameter routes.
 * @method void setStrParamArgumentStart(null|string $strParamArgumentStart) Set default argument start for parameter routes.
 * @method void setStrParamArgumentEnd(null|string $strParamArgumentEnd) Set default argument end for parameter routes.
 */
class DefaultConfig extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
        if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE, ConstConfig::DATA_DEFAULT_VALUE_PATTERN_DECORATION_REQUIRED);
        }

		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION, ConstConfig::DATA_DEFAULT_VALUE_PATTERN_DECORATION);
		}

        if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_START))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_START, ConstConfig::DATA_DEFAULT_VALUE_PARAM_ELEMENT_START);
        }

        if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_END))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_END, ConstConfig::DATA_DEFAULT_VALUE_PARAM_ELEMENT_END);
        }

		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_START))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_START, ConstConfig::DATA_DEFAULT_VALUE_PARAM_ARGUMENT_START);
		}
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_END))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_END, ConstConfig::DATA_DEFAULT_VALUE_PARAM_ARGUMENT_END);
		}
	}





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE,
            ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION,
            ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_START,
            ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_END,
            ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_START,
            ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_END
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE:
                    PatternDecorationRequireInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION:
                    PatternDecorationInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_START:
                case ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_START:
                    ParamStartInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_DEFAULT_PARAM_ELEMENT_END:
                case ConstConfig::DATA_KEY_DEFAULT_PARAM_ARGUMENT_END:
                ParamEndInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if pattern decoration is required.
     * This option allows to specify if by default a pattern route sources need to be decorated.
     *
     * @return boolean
     */
    public function checkPatternDecorationIsRequired()
    {
        // Return result
        return parent::beanGet(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE);
    }

	
	
}