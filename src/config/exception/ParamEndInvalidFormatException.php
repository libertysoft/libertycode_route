<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\config\exception;

use liberty_code\route\config\library\ConstConfig;



class ParamEndInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $end
     */
	public function __construct($end)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_PARAM_END_INVALID_FORMAT, strval($end));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified end has valid format.
	 * 
     * @param mixed $end
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($end)
    {
		// Init var
		$result =
            is_null($end) ||
            (is_string($end) && (trim($end) !== '')); // Check is valid string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($end);
		}
		
		// Return result
		return $result;
    }
	
	
	
}