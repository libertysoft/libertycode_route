<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\config\exception;

use liberty_code\route\config\library\ConstConfig;



class PatternDecorationInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $patternDeco
     */
	public function __construct($patternDeco)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_PATTERN_DECORATION_INVALID_FORMAT, strval($patternDeco));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified pattern decoration has valid format.
	 * 
     * @param mixed $patternDeco
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($patternDeco)
    {
		// Init var
		$result =
            is_null($patternDeco) || // Check is null
            (is_string($patternDeco) && (trim($patternDeco) !== '')); // Check is valid string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($patternDeco);
		}
		
		// Return result
		return $result;
    }
	
	
	
}