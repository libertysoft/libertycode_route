<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\config\exception;

use liberty_code\route\config\library\ConstConfig;



class ParamStartInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $start
     */
	public function __construct($start)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_PARAM_START_INVALID_FORMAT, strval($start));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified start has valid format.
	 * 
     * @param mixed $start
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($start)
    {
		// Init var
		$result =
            is_null($start) ||
            (is_string($start) && (trim($start) !== '')); // Check is valid string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($start);
		}
		
		// Return result
		return $result;
    }
	
	
	
}