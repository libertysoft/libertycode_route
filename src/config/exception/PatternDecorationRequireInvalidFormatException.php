<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\config\exception;

use liberty_code\route\config\library\ConstConfig;



class PatternDecorationRequireInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $patternDecoRequire
     */
	public function __construct($patternDecoRequire)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_PATTERN_DECORATION_REQUIRE_INVALID_FORMAT, strval($patternDecoRequire));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified pattern decoration require has valid format.
	 * 
     * @param mixed $patternDecoRequire
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($patternDecoRequire)
    {
		// Init var
		$result = is_bool($patternDecoRequire);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($patternDecoRequire);
		}
		
		// Return result
		return $result;
    }
	
	
	
}