<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
    // Data constants
    const DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE = 'boolPatternDecorationRequire';
    const DATA_KEY_DEFAULT_PATTERN_DECORATION = 'strPatternDecoration';
	const DATA_KEY_DEFAULT_PARAM_ELEMENT_START = 'strParamElementStart';
	const DATA_KEY_DEFAULT_PARAM_ELEMENT_END = 'strParamElementEnd';
    const DATA_KEY_DEFAULT_PARAM_ARGUMENT_START = 'strParamArgumentStart';
    const DATA_KEY_DEFAULT_PARAM_ARGUMENT_END = 'strParamArgumentEnd';

    const DATA_DEFAULT_VALUE_PATTERN_DECORATION_REQUIRED = true;
    const DATA_DEFAULT_VALUE_PATTERN_DECORATION = '#%1$s#';
    const DATA_DEFAULT_VALUE_PARAM_ELEMENT_START = null;
    const DATA_DEFAULT_VALUE_PARAM_ELEMENT_END = null;
	const DATA_DEFAULT_VALUE_PARAM_ARGUMENT_START = '{';
	const DATA_DEFAULT_VALUE_PARAM_ARGUMENT_END = '}';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_PATTERN_DECORATION_REQUIRE_INVALID_FORMAT = 'Following pattern decoration requirement option "%1$s" invalid! The requirement option must be a boolean.';
    const EXCEPT_MSG_PATTERN_DECORATION_INVALID_FORMAT = 'Following pattern decoration "%1$s" invalid! The pattern decoration must be a string, not empty.';
    const EXCEPT_MSG_PARAM_START_INVALID_FORMAT = 'Following item start "%1$s" invalid! The start must be a string, not empty.';
    const EXCEPT_MSG_PARAM_END_INVALID_FORMAT = 'Following item end "%1$s" invalid! The end must be a string, not empty.';
}