<?php
/**
 * Description :
 * This class allows to define default router class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\router\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\route\router\api\RouterInterface;

use liberty_code\route\config\model\DefaultConfig;
use liberty_code\route\route\api\RouteCollectionInterface;
use liberty_code\route\router\library\ConstRouter;
use liberty_code\route\router\exception\RouteCollectionInvalidFormatException;
use liberty_code\route\router\exception\RouteNotFoundException;
use liberty_code\route\router\exception\SourceUnableGetException;
use liberty_code\route\router\exception\CallableUnableGetException;



class DefaultRouter extends FixBean implements RouterInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RouteCollectionInterface $objRouteCollection
     */
    public function __construct(RouteCollectionInterface $objRouteCollection)
    {
        // Call parent constructor
        parent::__construct();

        // Init route collection
        $this->setRouteCollection($objRouteCollection);
    }



	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstRouter::DATA_KEY_DEFAULT_ROUTE_COLLECTION))
        {
            $this->__beanTabData[ConstRouter::DATA_KEY_DEFAULT_ROUTE_COLLECTION] = null;
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstRouter::DATA_KEY_DEFAULT_ROUTE_COLLECTION
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstRouter::DATA_KEY_DEFAULT_ROUTE_COLLECTION:
					RouteCollectionInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}



	
	
	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return $this->getObjRouteCollection()->checkExists($strKey);
    }



	/**
	 * @inheritdoc
	 */
	public function checkMatches($strSrc)
	{
		// Return result
		return $this->getObjRouteCollection()->checkMatches($strSrc);
	}

	
	
	
	
    // Methods getters
    // ******************************************************************************

    /**
     * Get route configuration.
     *
     * @return DefaultConfig
     */
    public function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



	/**
     * @inheritdoc
     */
    public function getObjRouteCollection()
    {
        // Return result
        return $this->beanGet(ConstRouter::DATA_KEY_DEFAULT_ROUTE_COLLECTION);
    }



    /**
     * @inheritdoc
     * @throws RouteNotFoundException
     * @throws SourceUnableGetException
     */
    public function getStrRouteHandleSource(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = null;

        // Check route found
        if($this->checkExists($strKey))
        {
            // Get info
            $objRoute = $this->getObjRouteCollection()->getObjRoute($strKey);
            $strSrc = $objRoute->getStrHandleSource($tabCallArg, $tabStrCallElm);

            // Check source valid
            if(!is_null($strSrc))
            {
                // Register source on result
                $result = $strSrc;
            }
            else
            {
                throw new SourceUnableGetException($strKey);
            }
        }
        else
        {
            throw new RouteNotFoundException($strKey);
        }

        // Return result
        return $result;
    }

	



    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setRouteCollection(RouteCollectionInterface $objRouteCollection)
	{
		$this->beanSet(ConstRouter::DATA_KEY_DEFAULT_ROUTE_COLLECTION, $objRouteCollection);
	}


	

	
	// Methods execute
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @return mixed[]
	 * @throws RouteNotFoundException
	 * @throws CallableUnableGetException
	 */
	public function execute($strSrc)
	{
		// Init var
		$result = array();
		
		// Check route found
		if($this->checkMatches($strSrc))
		{
			// Get info
			$tabCallable = $this->getObjRouteCollection()->getTabCallable($strSrc, true);
			
			// Run all callables
			foreach($tabCallable as $callable)
			{
				// Check callable valid
				if(!is_null($callable))
				{
					// Execute and register callable result
					$result[] = $callable();
				}
				else
				{
					throw new CallableUnableGetException($strSrc);
				}
			}
		}
		else
		{
			throw new RouteNotFoundException($strSrc);
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 * @throws RouteNotFoundException
	 * @throws CallableUnableGetException
	 */
	public function executeRoute(
	    $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
	{
		// Init var
		$result = null;
		
		// Check route found
		if($this->checkExists($strKey))
		{
			// Get info
			$objRoute = $this->getObjRouteCollection()->getObjRoute($strKey);
            $objCall = $objRoute->getObjCall();
			$callable = (
                (!is_null($objCall)) ?
                    $objCall->getCallable($tabCallArg, $tabStrCallElm) :
                    null
            );
			
			// Check callable valid
			if(!is_null($callable))
			{
				// Execute and register callable result
				$result = $callable();
			}
			else
			{
				throw new CallableUnableGetException($strKey);
			}
		}
		else
		{
			throw new RouteNotFoundException($strKey);
		}
		
		// Return result
		return $result;
	}
	
	
	
}