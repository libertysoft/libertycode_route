<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\router\exception;

use liberty_code\route\router\library\ConstRouter;



class CallableUnableGetException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $strRouteSrc
     */
	public function __construct($strRouteSrc)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRouter::EXCEPT_MSG_CALLABLE_UNABLE_GET,
            mb_strimwidth(strval($strRouteSrc), 0, 50, "...")
        );
	}
}