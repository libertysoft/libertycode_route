<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\router\exception;

use liberty_code\route\router\library\ConstRouter;
use liberty_code\route\route\api\RouteCollectionInterface;



class RouteCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $routeCollection
     */
	public function __construct($routeCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRouter::EXCEPT_MSG_ROUTE_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($routeCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified route collection has valid format
	 * 
     * @param mixed $routeCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($routeCollection)
    {
		// Init var
		$result = (
			(is_null($routeCollection)) ||
			($routeCollection instanceof RouteCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($routeCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}