<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\router\exception;

use liberty_code\route\router\library\ConstRouter;



class SourceUnableGetException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $strRouteKey
     */
	public function __construct($strRouteKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRouter::EXCEPT_MSG_SOURCE_UNABLE_GET,
            mb_strimwidth(strval($strRouteKey), 0, 50, "...")
        );
	}
}