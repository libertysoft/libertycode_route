<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/router/test/RouterTest.php');



// Init var
$tabDataSrc = array(
    'route_1_not_care' => [
        'type' => 'pattern',
        'key' => 'route_1',
        'source' => '^route_1/arg_1/[^/]+/arg_2/[^/]+/',
        'argument' => 'arg_\d/([^/]+)',
        'call' => [
            'class_path_pattern' => 'liberty_code\\route\\route\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ],
        'build_source_pattern' => 'route_1/arg_1/%1$s/arg_2/%2$s/build_test_1/'
    ],
    'route_2:[strAdd2]:test_1' => [
        'argument' => [
            'start' => '[',
            'end' => ']'
        ],
        'call' => [
            'type' => 'dependency',
            'dependency_key_pattern' => 'svc_1:action'
        ]
    ],
    'route_3' => [
        'type' => 'fix',
        'key' => 'route_3',
        'source' => 'route_3/test_1/test_2/test_3',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => 'src/route/test/FileControllerTest1.php',
            'file_path_format_require' => 7
        ]
    ],
    [
        'type' => 'separator',
        'key' => 'route_4',
        'separator' => '/',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
            'file_path_format_require' => true
        ]
    ],
    'route_5' => [
        'type' => 'pattern',
        'source' => '^route_5/controller/[^/]+/action/[^/]+/$',
        'element' => 'controller/([^/]+)/action/([^/]+)',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\route\\route\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
        ],
        'option' => [
            'decoration' => '5'
        ]
    ],
    [
        'type' => 'separator',
        'separator' => '/',
        'argument' => [
            'argument value 1'
        ],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => '/src/route/test/%1$sController%2$s.php',
            'file_path_format_require' => true
        ]
    ]
);

$objRouteCollection->setMultiMatch(false);
$objRouteCollection->setMultiMatch(true);

$objRouteBuilder->setTabDataSrc($tabDataSrc);
$objRouteBuilder->hydrateRouteCollection($objRouteCollection);



// Test check/get from source
$tabSrc = array(
	'route_1/arg_1/value arg 1/arg_2/value arg 2/test_1/' => [
	    [],
        []
    ], // Ok: Found route_1

	'route_1' => [
	    [],
	    ['strAdd2' => 'value arg 1 by key', 'strAdd' => 'value arg 2 by key']
    ], // Ok: Found route_1

	'route_2:value arg 1:test_1' => [
        [],
        []
    ], // Ok: Found route_2

    'route_3/test_1/test_2/test_3' => [
        [],
        []
    ], // Ok: Found route_3

    'route_3' => [
        [],
        ['value arg 1 by key']
    ], // Ok: Found route_3

    'File/Test1/test2' => [
        [],
        []
    ], // Ok: Found route_4

    'route_4' => [
        ['File'],
        ['value arg 1 by key']
    ], // Ko: Callable enable get

    'route_5/controller/Test2/action/action/' => [
        [],
        []
    ], // Ok: Found route_5

    'route_6/key/svc_1/method/action/{value arg 1}/test_1/{value arg 2}' => [
        [],
        []
    ], // Ko: Found route_6

	'test' => [
        [],
        []
    ], // Ko: Not found

	15 => [
        [],
        []
    ] // Ko: Not found
);

foreach($tabSrc as $strSrc => $tabInfo)
{
	echo('Test check, get source "'.$strSrc.'": <br />');
	try{
        $tabStrCallElm = $tabInfo[0];
        $tabCallArg = $tabInfo[1];
		$callResult =  null;

		if($objRouter->checkMatches($strSrc))
		{
			echo('Check source: true<br>');
			$callResult = $objRouter->execute($strSrc);
		}
		else if($objRouter->checkExists($strSrc))
		{
			echo('Check key: true<br>');
			
			$callResult = $objRouter->executeRoute($strSrc, $tabCallArg, $tabStrCallElm);
		}
		else
		{
			echo('Source or key not found<br />');
		}
		
		echo('Call: callable result: <pre>');var_dump($callResult);echo('</pre>');

        try{
            $strHandleSrc = $objRouter->getStrRouteHandleSource($strSrc, $tabCallArg, $tabStrCallElm);
            echo('Handle source: <pre>');var_dump($strHandleSrc);echo('</pre>');
        } catch(\Exception $e) {
            echo('Handle source not found<br />');
        }

	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


