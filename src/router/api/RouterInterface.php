<?php
/**
 * Description :
 * This class allows to describe behavior of router class.
 * Router allows to execute callback function(s),
 * from specified source or route key, using route collection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\router\api;

use liberty_code\route\route\api\RouteCollectionInterface;



interface RouterInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

	/**
	 * Check if specified route key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);



    /**
     * Check if specified source matches with at least one route.
     *
     * @param string $strSrc
     * @return boolean
     */
    public function checkMatches($strSrc);



	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get route collection object.
	 *
	 * @return RouteCollectionInterface
	 */
	public function getObjRouteCollection();



    /**
     * Get route source,
     * from specified route key,
     * and specified array of arguments (for call destination),
     * and specified array of string elements (for call destination customization).
     *
     * @param string $strKey
     * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
     * @return string
     */
    public function getStrRouteHandleSource(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set route collection object.
	 * 
	 * @param RouteCollectionInterface $objRouteCollection
	 */
	public function setRouteCollection(RouteCollectionInterface $objRouteCollection);
	
	
	
	
	
	// Methods execute
	// ******************************************************************************
	
	/**
	 * Execute specified source.
	 * 
	 * @param string $strSrc
	 * @return mixed
	 */
	public function execute($strSrc);
	
	
	
	/**
	 * Execute specified route key,
     * from specified array of arguments (for call destination),
     * and specified array of string elements (for call destination customization).
	 * 
	 * @param string $strKey
	 * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
	 * @return mixed
	 */
	public function executeRoute(
	    $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );
}