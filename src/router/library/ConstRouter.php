<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\route\router\library;



class ConstRouter
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_DEFAULT_ROUTE_COLLECTION = 'objRouteCollection';



    // Exception message constants
    const EXCEPT_MSG_ROUTE_COLLECTION_INVALID_FORMAT = 'Following route collection "%1$s" invalid! It must be a route collection object.';
	const EXCEPT_MSG_ROUTE_NOT_FOUND = 'No route found from following source "%1s"!';
	const EXCEPT_MSG_SOURCE_UNABLE_GET = 'Impossible to get source from following route "%1$s"! Some route configuration elements are unsolved.';
    const EXCEPT_MSG_CALLABLE_UNABLE_GET = 'Impossible to get callable from following route or source "%1$s"! Some route configuration elements are unsolved.';
}